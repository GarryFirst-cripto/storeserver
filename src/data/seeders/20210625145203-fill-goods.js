import { goodsSeed, groupsSeed } from './seed-data/goodsSeed';
import { naborsSeed, naborsItemSeed } from './seed-data/naborSeed';

export default {
  up: async queryInterface => {
    try {
      const data = new Date();

      const goodsMappedSeed = goodsSeed.map(item => ({
        ...item, createdAt: data
      }));
      await queryInterface.bulkInsert('goods', goodsMappedSeed, {});

      const groupsMappedSeed = groupsSeed.map(item => ({
        ...item, createdAt: data
      }));
      await queryInterface.bulkInsert('groups', groupsMappedSeed, {});

      const naborsMappedSeed = naborsSeed.map(item => ({
        ...item, createdAt: data
      }));
      await queryInterface.bulkInsert('nabors', naborsMappedSeed, {});

      const naborsItemMappedSeed = naborsItemSeed.map(item => ({
        ...item, createdAt: data
      }));
      await queryInterface.bulkInsert('naboritems', naborsItemMappedSeed, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('goods', null, {});
      await queryInterface.bulkDelete('groups', null, {});
      await queryInterface.bulkDelete('naboritems', null, {});
      await queryInterface.bulkDelete('nabors', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }
};
