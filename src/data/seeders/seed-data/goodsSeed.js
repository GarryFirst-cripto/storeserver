export const goodsSeed = [
  {
    id: '7af64d50-c003-4be5-aea6-2c1eb60ea04e',
    goodname: 'Vegetable A',
    price: 24.5,
    discount: 0,
    index: 0,
    info: 'Vegetable A Info Info',
    balans: 15
  },
  {
    id: 'e2f06b0e-f888-46da-990d-c03f53ebbaa4',
    goodname: 'Vegetable B',
    price: 12.45,
    discount: 0,
    index: 0,
    info: 'Vegetable B Info Info',
    balans: 10
  },
  {
    id: 'cd7f5455-c6a7-42cb-9860-cde3d5437b16',
    goodname: 'Vegetable C',
    price: 15,
    discount: 10,
    index: 1,
    info: 'Vegetable B Info Info',
    balans: 10
  },
  {
    id: '48536898-d174-44bd-bdcd-f7f7aa782340',
    goodname: 'Vegetable D',
    price: 16,
    discount: 5,
    index: 0,
    info: 'Vegetable D Info Info',
    balans: 45
  },
  {
    id: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    goodname: 'Fruit AA',
    price: 45,
    discount: 0,
    index: 0,
    info: 'Fruit AA Info Info',
    balans: 100,
    groupp: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa, c37d8d5d-0205-4bfe-8ce2-4136a1ce6391'
  },
  {
    id: 'b2c4cf2d-19b1-4fa0-a16f-6e7111ee9515',
    goodname: 'Fruit AB',
    price: 5,
    discount: 0,
    index: 0,
    info: 'Fruit AB Info Info',
    balans: 100,
    groupp: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa, c37d8d5d-0205-4bfe-8ce2-4136a1ce6391'
  },
  {
    id: 'dc8e0765-6d24-4a31-800c-ba6d8e1c6ce5',
    goodname: 'Fruit BB',
    price: 35,
    discount: 0,
    index: 0,
    info: 'Fruit B-B Info Info',
    balans: 140,
    groupp: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa, cc807db3-39cd-441d-8256-44aa9192409c'
  },
  {
    id: 'c1055066-d75b-4e22-b15c-e4baf453536b',
    goodname: 'Juice A',
    price: 135,
    discount: 10,
    index: 0,
    info: 'Juice Fruit A Info Info',
    balans: 40,
    groupp: '1e6d3f93-684e-471a-a9e2-3ee49c924c53, 93c548c8-7d1b-45e6-be3a-401c296e6254, ce4de00a-422d-4f36-8ac6-0fd27481fe11'
  },
  {
    id: '9f912064-fc9c-4b2f-abe7-d22415de86cd',
    goodname: 'Juice B',
    price: 90.5,
    discount: 10,
    index: 0,
    info: 'Juice Fruit B Info Info',
    balans: 80,
    groupp: '1e6d3f93-684e-471a-a9e2-3ee49c924c53, 93c548c8-7d1b-45e6-be3a-401c296e6254, ce4de00a-422d-4f36-8ac6-0fd27481fe11, 1d38c664-4b1a-4a80-8991-4a6b7c93f779'
  },
  {
    id: '05e8ae1e-64f3-4abe-8828-bc671375576a',
    goodname: 'Fresh Drink A',
    price: 70.5,
    discount: 0,
    index: 0,
    info: 'Fresh Drink A Info Info',
    balans: 80,
    groupp: '1e6d3f93-684e-471a-a9e2-3ee49c924c53, 93c548c8-7d1b-45e6-be3a-401c296e6254, ce4de00a-422d-4f36-8ac6-0fd27481fe11'
  },
  {
    id: '538bbc08-799d-49ba-a7e8-383ede0138e3',
    goodname: 'Fresh Drink B',
    price: 50,
    discount: 0,
    index: 0,
    info: 'Fresh Drink B Info Info',
    balans: 100,
    groupp: '1e6d3f93-684e-471a-a9e2-3ee49c924c53, 93c548c8-7d1b-45e6-be3a-401c296e6254, ce4de00a-422d-4f36-8ac6-0fd27481fe11, b73b3537-3dda-4864-a58b-3bbc6377e2f3'
  },
  {
    id: 'e9e6c1df-9313-4b6b-947c-506d10cfaee6',
    goodname: 'Fresh Drink C',
    price: 65,
    discount: 0,
    index: 0,
    info: 'Fresh Drink C Info Info Info',
    balans: 100,
    groupp: '1e6d3f93-684e-471a-a9e2-3ee49c924c53, 93c548c8-7d1b-45e6-be3a-401c296e6254, ce4de00a-422d-4f36-8ac6-0fd27481fe11'
  }
];

export const groupsSeed = [
  {
    id: '1e6d3f93-684e-471a-a9e2-3ee49c924c53',
    parentId: '',
    groupname: 'Drinks',
    index: 0
  },
  {
    id: '93c548c8-7d1b-45e6-be3a-401c296e6254',
    parentId: '1e6d3f93-684e-471a-a9e2-3ee49c924c53',
    groupname: 'Drinks - 1',
    index: 0
  },
  {
    id: 'c71f6a9c-ed7e-4de9-993e-d70664fecaff',
    parentId: '93c548c8-7d1b-45e6-be3a-401c296e6254',
    groupname: 'Drinks - 1 - 1',
    index: 0
  },
  {
    id: 'ce4de00a-422d-4f36-8ac6-0fd27481fe11',
    parentId: '93c548c8-7d1b-45e6-be3a-401c296e6254',
    groupname: 'Drinks - 1 - 2',
    index: 0
  },
  {
    id: '32ccfe85-5d75-4959-835f-cf02ea6d10db',
    parentId: '93c548c8-7d1b-45e6-be3a-401c296e6254',
    groupname: 'Drinks - 1 - 3',
    index: 0
  },
  {
    id: '0ae26b61-eb07-4c93-9341-282f8a352b29',
    parentId: '1e6d3f93-684e-471a-a9e2-3ee49c924c53',
    groupname: 'Drinks - 2',
    index: 0
  },
  {
    id: 'eaf39303-3069-46ce-8a5f-91953072d7aa',
    parentId: '',
    groupname: 'Fruits - Common',
    index: 0
  },
  {
    id: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa',
    parentId: '',
    groupname: 'Fruits - Fresh',
    index: 0
  },
  {
    id: 'c37d8d5d-0205-4bfe-8ce2-4136a1ce6391',
    parentId: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa',
    groupname: 'Fruits - Fresh - A',
    index: 0
  },
  {
    id: 'cc807db3-39cd-441d-8256-44aa9192409c',
    parentId: 'dcc2fe49-c0bc-496f-b0a6-63bbfa109cfa',
    groupname: 'Fruits - Fresh - B',
    index: 0
  },
  {
    id: '1d38c664-4b1a-4a80-8991-4a6b7c93f779',
    parentId: 'ce4de00a-422d-4f36-8ac6-0fd27481fe11',
    groupname: 'Fruits Drinks - XXX',
    index: 0
  },
  {
    id: 'b73b3537-3dda-4864-a58b-3bbc6377e2f3',
    parentId: 'ce4de00a-422d-4f36-8ac6-0fd27481fe11',
    groupname: 'Fruits Drinks - YYY',
    index: 0
  }
];
