export const usersSeed = [
  {
    id: 'fedfccc1-73ca-4055-ab52-1e501988bf56',
    phone: '+234 3315611',
    username: 'Tommy Kolton'
  },
  {
    id: 'b067e52d-a394-495a-bed0-bf4e0831c1a6',
    phone: '+555 3344555',
    username: 'Tommy Tommy',
    pushId: 'xxxx xxxx xxxx'
  },
  {
    id: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9',
    phone: '+234 4415600',
    username: 'Tommy Bob',
    status: false
  },
  {
    id: 'f664ddc9-6c64-4c2e-a637-5ecc3d2b8a32',
    phone: '+234 3315612',
    username: 'Mary Kolton',
    pushId: 'yyyy yyyy yyyy'
  },
  {
    id: 'bea64e2c-8cf8-4bfb-a8ca-31a7f8e86f83',
    phone: '+234 5515651',
    username: 'Billy Bob',
    info: 'Info Info Info',
    balans: 100,
    bonus: 23.5
  }
];

export const paysSeed = [
  {
    id: 'e64b3434-9556-4fa3-81fc-4156110374e7',
    userId: 'b067e52d-a394-495a-bed0-bf4e0831c1a6',
    cartName: 'Card Card',
    number: 2345678900001111,
    srok: '2021-06-24T20:01:44.433Z',
    cvc: 456
  },
  {
    id: '64833bbd-1d61-43ca-80ba-23e944de4e1a',
    userId: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9',
    cartName: 'Card Card Card',
    number: 2345678900002222,
    srok: '2021-06-24T20:01:44.433Z',
    cvc: 123
  },
  {
    id: '6b89ef39-842f-401b-8667-eb0f91640427',
    userId: 'bea64e2c-8cf8-4bfb-a8ca-31a7f8e86f83',
    cartName: 'First Card Gold',
    number: 1245678900003333,
    srok: '2021-06-24T20:01:44.433Z',
    cvc: 234
  },
  {
    id: 'e7ea1005-32de-4707-a2cf-13c9280295da',
    userId: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9',
    cartName: 'Second Card',
    number: 1245678900002222,
    srok: '2021-06-24T20:01:44.433Z',
    cvc: 123
  },
  {
    id: 'bbab0985-d14c-4e58-bc3f-91824e6d76f5',
    userId: 'f664ddc9-6c64-4c2e-a637-5ecc3d2b8a32',
    cartName: 'Second Card Gold',
    number: 1245678900003333,
    srok: '2021-06-24T20:01:44.433Z',
    cvc: 234
  }
];

export const addrsSeed = [
  {
    id: '5a4eb546-0088-4ab7-8001-c1ef19f9bf42',
    userId: 'b067e52d-a394-495a-bed0-bf4e0831c1a6',
    city: 'Big City',
    address: 'Sadovaya street, 10',
    dopInfo: 'Info info info info info ....'
  },
  {
    id: 'ed5d5ea6-ca00-4607-9325-57c6dc3a5e6c',
    userId: 'b067e52d-a394-495a-bed0-bf4e0831c1a6',
    city: 'Big City',
    address: 'Sadovaya street, 10'
  },
  {
    id: '655f84d9-6d7a-47c9-9d67-58ebc07249ac',
    userId: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9',
    city: 'Big City',
    address: 'Parkovaya street, 15'
  },
  {
    id: 'c2ebb3e6-ff7d-4ed8-9312-7879aab0c689',
    userId: 'f664ddc9-6c64-4c2e-a637-5ecc3d2b8a32',
    city: 'Big City',
    address: 'Sadovaya street, 15',
    dopInfo: 'DopInfo info info info DopInfo'
  },
  {
    id: 'e6886bed-d1d2-4dcb-b0bb-44e28ff033ef',
    userId: 'f664ddc9-6c64-4c2e-a637-5ecc3d2b8a32',
    city: 'Super Big City',
    address: 'Kolhoznaya street, 15',
    dopInfo: 'Info info info info'
  },
  {
    id: 'be0dbd8f-2d5e-49d9-a9f4-6fae741cc34e',
    userId: 'bea64e2c-8cf8-4bfb-a8ca-31a7f8e86f83',
    city: 'Super Big City',
    address: 'Kolhoznaya street, 27',
    dopInfo: 'Info info info info'
  }
];
