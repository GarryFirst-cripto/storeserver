export const zakazSeed = [
  {
    pay: {
      cartName: 'My first Card',
      number: 2345678908762345,
      srok: '10/24',
      cvc: 244
    },
    address: [
      {
        city: 'Big - big city',
        address: 'Parkovaya street, 10',
        dopInfo: 'Dop dop dop info'
      }, {
        city: 'Big - big city',
        address: 'Sadovaya street, 10',
        dopInfo: 'Dop dop dop info'
      }
    ],
    id: 'c8f7ff47-48c4-47a2-855e-aefaed744475',
    userId: 'bea64e2c-8cf8-4bfb-a8ca-31a7f8e86f83'
  }, {
    pay: {
      cartName: 'My first Card',
      number: 2345678908762345,
      srok: '10/24',
      cvc: 244
    },
    address: {
      city: 'Big - big city',
      address: 'Parkovaya street, 10',
      dopInfo: 'Dop dop dop info'
    },
    id: '995a5edc-c0b1-4e69-9049-67979b755b9d',
    userId: 'bea64e2c-8cf8-4bfb-a8ca-31a7f8e86f83'
  }, {
    pay: {
      cartName: 'My second Card',
      number: 2345678908762345,
      srok: '11/22',
      cvc: 333
    },
    address: {
      city: 'My own city',
      address: 'Stepnaya street, 1',
      dopInfo: 'Dop dop dop info'
    },
    id: '07ae1e1e-d1e1-4f62-951c-eea9a81d9269',
    userId: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9'
  }, {
    pay: {
      cartName: 'My first Card',
      number: 2345678908762345,
      srok: '10/24',
      cvc: 244
    },
    address: {
      city: 'Big - big city',
      address: 'Parkovaya street, 10',
      dopInfo: 'Dop dop dop info'
    },
    id: 'c9f22836-b016-4ae9-8b3d-4b5e26ae43d5',
    userId: '0a05cb68-bd84-40e6-9270-1b2e3d5791b9'
  }
];

export const zakazItemSeed = [
  {
    id: '6e173aaf-9839-43ed-a784-f5355ccbd8b2',
    zakazId: 'c8f7ff47-48c4-47a2-855e-aefaed744475',
    goodId: '7af64d50-c003-4be5-aea6-2c1eb60ea04e',
    kolich: 1,
    price: 15.5
  }, {
    id: '8c4653cc-106d-4c2d-934e-fd736c1e70cd',
    zakazId: 'c8f7ff47-48c4-47a2-855e-aefaed744475',
    goodId: 'e2f06b0e-f888-46da-990d-c03f53ebbaa4',
    kolich: 2,
    price: 25.5
  }, {
    id: '34e6c1cb-11c7-4d6f-a2ec-a4167f4f6813',
    zakazId: 'c8f7ff47-48c4-47a2-855e-aefaed744475',
    naborId: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    kolich: 1,
    price: 85.5
  }, {
    id: '8a8aca3c-91f4-40d9-ad38-7c41a94db4f9',
    zakazId: '995a5edc-c0b1-4e69-9049-67979b755b9d',
    goodId: 'cd7f5455-c6a7-42cb-9860-cde3d5437b16',
    kolich: 1,
    price: 15.5
  }, {
    id: '8d92eac8-e4b7-4290-9eff-be9a99c9ebae',
    zakazId: '995a5edc-c0b1-4e69-9049-67979b755b9d',
    goodId: '48536898-d174-44bd-bdcd-f7f7aa782340',
    kolich: 2,
    price: 25.5
  }, {
    id: '34d2a250-c5ca-4c42-b377-9cef3cb0c673',
    zakazId: '995a5edc-c0b1-4e69-9049-67979b755b9d',
    naborId: '026dd75f-cfae-4786-bf52-5780d35627aa',
    kolich: 2,
    price: 110
  }, {
    id: '5b751840-6323-4a80-940d-7f1f64887326',
    zakazId: '07ae1e1e-d1e1-4f62-951c-eea9a81d9269',
    goodId: 'cd7f5455-c6a7-42cb-9860-cde3d5437b16',
    kolich: 1,
    price: 15.5
  }, {
    id: '3cea6b60-ba83-4c24-8493-2d88171b323d',
    zakazId: '07ae1e1e-d1e1-4f62-951c-eea9a81d9269',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    kolich: 5,
    price: 25.5
  }, {
    id: '5cfd4e54-4406-4638-86aa-56f8e11adeec',
    zakazId: '07ae1e1e-d1e1-4f62-951c-eea9a81d9269',
    goodId: '48536898-d174-44bd-bdcd-f7f7aa782340',
    kolich: 2,
    price: 25.5
  }, {
    id: '3a7d4856-87f0-4808-a8d4-59e2b445602e',
    zakazId: '07ae1e1e-d1e1-4f62-951c-eea9a81d9269',
    naborId: '026dd75f-cfae-4786-bf52-5780d35627aa',
    kolich: 2,
    price: 110
  }, {
    id: '6e8cf05b-01d2-4da1-a150-5bd23687ab3e',
    zakazId: 'c9f22836-b016-4ae9-8b3d-4b5e26ae43d5',
    goodId: null,
    naborId: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    kolich: 2,
    price: 110
  }
];
