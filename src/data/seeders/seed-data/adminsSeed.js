export const adminsSeed = [
  {
    id: 'e58dc431-9cdc-4e97-8590-719456ca9a26',
    phone: '+444 5515551',
    username: 'Billy Jhons'
  },
  {
    id: '8df51624-b3d2-4242-b340-b78e5552fb54',
    phone: '+555 5515551',
    username: 'Billy Tomas'
  },
  {
    id: '5f7ed746-4ba8-4604-9b92-dac3f207b13a',
    phone: '+555 0012221',
    username: 'Jhon Tomas'
  }
];
