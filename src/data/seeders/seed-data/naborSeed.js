export const naborsSeed = [
  {
    id: '6d9d5624-39e3-4624-95de-84e020c6f992',
    naborname: 'Fresh Drinks',
    price: 165,
    discount: 0,
    index: 0,
    info: 'Fresh Drinks Fresh Drinks Fresh Drinks'
  },
  {
    id: '026dd75f-cfae-4786-bf52-5780d35627aa',
    naborname: 'Fresh Fruits',
    price: 200,
    index: 1,
    info: 'Fresh Fruits - Fresh Drinks  - Fresh Fruits'
  },
  {
    id: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    naborname: 'Vegetables',
    price: 120,
    discount: 10,
    index: 2,
    info: 'Vegetables Vegetables Vegetables Vegetables'
  },
  {
    id: '6db4298c-9786-428d-bfe1-4c6df688b74e',
    naborname: 'Vegetables - Dopp',
    price: 140,
    discount: 0,
    index: 2,
    info: 'Vegetables Vegetables Vegetables Vegetables'
  }
];

export const naborsItemSeed = [
  {
    id: 'ca9879f9-27d8-48a6-b5be-3df584519a10',
    naborId: '6d9d5624-39e3-4624-95de-84e020c6f992',
    goodId: 'cd7f5455-c6a7-42cb-9860-cde3d5437b16',
    price: 15
  },
  {
    id: '2c49cde5-d3b2-41bd-a430-5d00df076c75',
    naborId: '6d9d5624-39e3-4624-95de-84e020c6f992',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 50
  },
  {
    id: '65031ba3-941e-4be4-b6b5-4c12f0045327',
    naborId: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 50
  },
  {
    id: '6de644a9-bf30-4dea-af3b-c161a92dad9a',
    naborId: '6db4298c-9786-428d-bfe1-4c6df688b74e',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 50
  },
  {
    id: 'ec25dc5c-a87a-495b-9a9f-b4296cb22f5d',
    naborId: '6d9d5624-39e3-4624-95de-84e020c6f992',
    goodId: 'e2f06b0e-f888-46da-990d-c03f53ebbaa4',
    price: 45
  },
  {
    id: '9ec2b234-8e5c-4294-868d-cf5db249937e',
    naborId: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    goodId: 'e2f06b0e-f888-46da-990d-c03f53ebbaa4',
    price: 45
  },
  {
    id: '4300d0cb-28cc-4da6-81a9-c173f6617892',
    naborId: '6db4298c-9786-428d-bfe1-4c6df688b74e',
    goodId: 'e2f06b0e-f888-46da-990d-c03f53ebbaa4',
    price: 45
  },
  {
    id: 'd8cc7058-53f7-44ac-92c8-eb4a6cbc378f',
    naborId: '6d9d5624-39e3-4624-95de-84e020c6f992',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 35
  },
  {
    id: '66723d93-cf1c-408c-9a21-d33384710bc0',
    naborId: 'e300b5e2-8ebf-400b-ab9e-1e982b762eed',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 35
  },
  {
    id: '566e0495-9867-45ee-841b-5e7d77e7b207',
    naborId: '6db4298c-9786-428d-bfe1-4c6df688b74e',
    goodId: '5edf1a33-6312-42a1-abd0-1789fb4e35a1',
    price: 35
  }
];
