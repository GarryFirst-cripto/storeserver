import { usersSeed, paysSeed, addrsSeed } from './seed-data/usersSeed';
import { adminsSeed } from './seed-data/adminsSeed';

export default {
  up: async queryInterface => {
    try {
      const data = new Date();

      const usersMappedSeed = usersSeed.map(user => ({
        ...user, createdAt: data
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});

      // Add users Pays.
      const paysMappedSeed = paysSeed.map(pay => ({
        ...pay, createdAt: data
      }));
      await queryInterface.bulkInsert('pays', paysMappedSeed, {});

      // Add users Address.
      const addrsMappedSeed = addrsSeed.map(addr => ({
        ...addr, createdAt: data
      }));
      await queryInterface.bulkInsert('addrs', addrsMappedSeed, {});

      // Add admins.
      const adminMappedSeed = adminsSeed.map(admin => ({
        ...admin, createdAt: data
      }));
      await queryInterface.bulkInsert('admins', adminMappedSeed, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('pays', null, {});
      await queryInterface.bulkDelete('addrs', null, {});
      await queryInterface.bulkDelete('users', null, {});
      await queryInterface.bulkDelete('admins', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }
};
