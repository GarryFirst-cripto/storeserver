import { zakazSeed, zakazItemSeed } from './seed-data/zakazSeed';

export default {
  up: async queryInterface => {
    try {
      const data = new Date();

      const zakazMappedSeed = zakazSeed.map(item => {
        const { id, userId, adminId, pay: payData, address: addressData, summa, srok } = item;
        const pay = JSON.stringify(payData);
        const address = JSON.stringify(addressData);
        const result = { id, userId, adminId, pay, address, summa, srok, createdAt: data };
        return result;
      });
      await queryInterface.bulkInsert('zakazs', zakazMappedSeed, {});

      const zakazItemMappedSeed = zakazItemSeed.map(item => ({
        ...item, createdAt: data
      }));
      await queryInterface.bulkInsert('zakazitems', zakazItemMappedSeed, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('zakazitems', null, {});
      await queryInterface.bulkDelete('zakazs', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }
};
