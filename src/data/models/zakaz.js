import { DataTypes } from 'sequelize';

export default orm => {
  const Zakaz = orm.define('zakaz', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    adminId: {
      allowNull: true,
      type: DataTypes.STRING
    },
    crm_id: DataTypes.BIGINT,
    // pay: {
    //   type: DataTypes.STRING,
    //   get() {
    //     try {
    //       return JSON.parse(this.getDataValue('pay'));
    //     } catch {
    //       return null;
    //     }
    //   },
    //   set(value) {
    //     try {
    //       return this.setDataValue('pay', JSON.stringify(value));
    //     } catch {
    //       return this.setDataValue('pay', null);
    //     }
    //   }
    // },
    // address: {
    //   type: DataTypes.TEXT,
    //   get() {
    //     try {
    //       return JSON.parse(this.getDataValue('address'));
    //     } catch {
    //       return null;
    //     }
    //   },
    //   set(value) {
    //     try {
    //       return this.setDataValue('address', JSON.stringify(value));
    //     } catch {
    //       return this.setDataValue('address', null);
    //     }
    //   }
    // },
    number: DataTypes.BIGINT,
    summa: DataTypes.FLOAT,
    discount: DataTypes.NUMBER,
    srokStart: DataTypes.DATE,
    srokEnd: DataTypes.DATE,
    info: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Zakaz;
};
