import { DataTypes } from 'sequelize';

export default orm => {
  const User = orm.define('user', {
    phone: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    crm_id: DataTypes.BIGINT,
    pushId: DataTypes.STRING,
    status: DataTypes.STRING,
    info: DataTypes.STRING,
    balans: DataTypes.NUMBER,
    bonus: DataTypes.NUMBER,
    verify: DataTypes.STRING,
    confirmed: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
