import { DataTypes } from 'sequelize';

export default orm => {
  const NaborItem = orm.define('naboritem', {
    naborId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    goodId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    price: DataTypes.NUMBER,
    discount: DataTypes.NUMBER,
    kolich: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return NaborItem;
};
