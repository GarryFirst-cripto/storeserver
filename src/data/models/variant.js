import { DataTypes } from 'sequelize';

export default orm => {
  const Variant = orm.define('variant', {
    goodId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    kolich: DataTypes.FLOAT,
    price: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Variant;
};
