export default models => {
  const {
    User,
    Pay,
    Address,
    Good,
    Variant,
    Nabor,
    NaborItem,
    Zakaz,
    ZakazItem,
    PromoKod
  } = models;

  User.hasMany(Pay);
  User.hasMany(Address);
  Pay.belongsTo(User);
  Pay.belongsTo(Zakaz);
  Address.belongsTo(User);
  Address.belongsTo(Zakaz);
  Nabor.hasMany(NaborItem);
  Good.hasMany(NaborItem);
  Good.hasMany(Variant);
  Variant.belongsTo(Good);
  NaborItem.belongsTo(Nabor);
  NaborItem.belongsTo(Good);
  Zakaz.hasMany(ZakazItem);
  Zakaz.hasOne(Pay);
  Zakaz.hasOne(Address);
  ZakazItem.belongsTo(Zakaz);
  ZakazItem.belongsTo(Good);
  ZakazItem.belongsTo(Nabor);
  Nabor.hasMany(ZakazItem);
  Good.hasMany(ZakazItem);
  User.hasMany(Zakaz);
  Zakaz.belongsTo(User);
  PromoKod.belongsTo(Good);
  Good.hasMany(PromoKod);
};
