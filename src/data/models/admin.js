import { DataTypes } from 'sequelize';

export default orm => {
  const Admin = orm.define('admin', {
    phone: {
      type: DataTypes.STRING,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      get() {
        try {
          return this.getDataValue('email').toLowerCase();
        } catch {
          return null;
        }
      },
      set(value) {
        return this.setDataValue('email', value.toLowerCase());
      }
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    privilage: DataTypes.BOOLEAN,
    verify: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Admin;
};
