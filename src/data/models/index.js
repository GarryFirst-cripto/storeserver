import sequelize from '../db/connection';
import associate from './associations';
import userModel from './user';
import adminModel from './admin';
import paysModel from './pays';
import addressModel from './address';
import goodModel from './good';
import variantModel from './variant';
import groupModel from './group';
import naborModel from './nabor';
import naborItemModel from './naboritem';
import zakazModel from './zakaz';
import zakazItemModel from './zakazitem';
import promoKodModel from './promokod';
import paytypeModel from './paytype';

const User = userModel(sequelize);
const Admin = adminModel(sequelize);
const Pay = paysModel(sequelize);
const Address = addressModel(sequelize);
const Good = goodModel(sequelize);
const Variant = variantModel(sequelize);
const Group = groupModel(sequelize);
const Nabor = naborModel(sequelize);
const NaborItem = naborItemModel(sequelize);
const Zakaz = zakazModel(sequelize);
const ZakazItem = zakazItemModel(sequelize);
const PromoKod = promoKodModel(sequelize);
const PayType = paytypeModel(sequelize);

associate({
  User,
  Pay,
  Address,
  Good,
  Variant,
  Nabor,
  NaborItem,
  Zakaz,
  ZakazItem,
  PromoKod
});

export {
  User as UserModel,
  Admin as AdminModel,
  Pay as PayModel,
  Address as AddressModel,
  Good as GoodModel,
  Variant as VariantModel,
  Group as GroupModel,
  Nabor as NaborModel,
  NaborItem as NaborItemModel,
  Zakaz as ZakazModel,
  ZakazItem as ZakazItemModel,
  PromoKod as PromoKodModel,
  PayType as PaytypeModel
};
