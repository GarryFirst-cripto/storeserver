import { DataTypes } from 'sequelize';

export default orm => {
  const Nabor = orm.define('nabor', {
    naborname: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    price: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    discount: DataTypes.NUMBER,
    firstdata: DataTypes.DATE,
    lastdata: DataTypes.DATE,
    index: DataTypes.NUMBER,
    info: DataTypes.STRING,
    picture: DataTypes.STRING,
    groupp: DataTypes.STRING,
    tags: DataTypes.STRING,
    mode: DataTypes.STRING,
    hidden: DataTypes.BOOLEAN,
    keytext: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Nabor;
};
