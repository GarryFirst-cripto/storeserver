import { DataTypes } from 'sequelize';

export default orm => {
  const Good = orm.define('good', {
    goodname: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    price: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    crm_id: DataTypes.BIGINT,
    discount: DataTypes.NUMBER,
    firstdata: DataTypes.DATE,
    lastdata: DataTypes.DATE,
    index: DataTypes.NUMBER,
    info: DataTypes.STRING,
    balans: DataTypes.NUMBER,
    unit: DataTypes.STRING,
    hidden: DataTypes.BOOLEAN,
    picture: DataTypes.STRING,
    groupp: DataTypes.STRING,
    tags: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Good;
};
