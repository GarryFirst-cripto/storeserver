import { DataTypes } from 'sequelize';

export default orm => {
  const Pays = orm.define('pay', {
    userId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    zakazId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    cartName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    number: {
      allowNull: false,
      type: DataTypes.BIGINT
    },
    srok: {
      allowNull: false,
      type: DataTypes.STRING
    },
    cvc: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Pays;
};
