import { DataTypes } from 'sequelize';

export default orm => {
  const ZakazItem = orm.define('zakazitem', {
    zakazId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    goodId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    naborId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    kolich: DataTypes.FLOAT,
    price: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return ZakazItem;
};
