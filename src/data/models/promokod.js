import { DataTypes } from 'sequelize';

export default orm => {
  const PromoKod = orm.define('promokod', {
    kodname: DataTypes.STRING,
    mode: DataTypes.INTEGER,
    totalZakaz: DataTypes.NUMBER,
    goodDiscount: DataTypes.NUMBER,
    freeGood: DataTypes.NUMBER,
    goodId: DataTypes.UUID,
    hidden: DataTypes.BOOLEAN,
    busket: DataTypes.BOOLEAN,
    firstdata: DataTypes.DATE,
    lastdata: DataTypes.DATE,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return PromoKod;
};
