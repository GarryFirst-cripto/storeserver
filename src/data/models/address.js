import { DataTypes } from 'sequelize';

export default orm => {
  const Address = orm.define('addr', {
    userId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    zakazId: {
      allowNull: true,
      type: DataTypes.UUID
    },
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT,
    mailindex: DataTypes.INTEGER,
    region: DataTypes.STRING,
    city: DataTypes.STRING,
    street: DataTypes.STRING,
    house: DataTypes.STRING,
    flat: DataTypes.INTEGER,
    korpus: DataTypes.STRING,
    floor: DataTypes.INTEGER,
    dopInfo: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Address;
};
