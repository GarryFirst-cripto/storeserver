import { DataTypes } from 'sequelize';

export default orm => {
  const PromoKod = orm.define('paytype', {
    payname: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    mode: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return PromoKod;
};
