import { DataTypes } from 'sequelize';

export default orm => {
  const Grupp = orm.define('group', {
    parentId: DataTypes.STRING,
    groupname: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    index: DataTypes.NUMBER,
    tags: DataTypes.STRING,
    picture: DataTypes.STRING,
    hidden: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Grupp;
};
