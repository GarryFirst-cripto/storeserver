export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('goods', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        goodname: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        price: {
          allowNull: false,
          type: Sequelize.FLOAT
        },
        discount: Sequelize.FLOAT,
        index: Sequelize.INTEGER,
        info: Sequelize.STRING,
        balans: Sequelize.FLOAT,
        picture: Sequelize.STRING,
        groupp: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('groups', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        parentId: Sequelize.STRING,
        groupname: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('nabors', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        naborname: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        price: {
          allowNull: false,
          type: Sequelize.FLOAT
        },
        discount: Sequelize.FLOAT,
        index: Sequelize.INTEGER,
        info: Sequelize.STRING,
        picture: Sequelize.STRING,
        groupp: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('naboritems', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        price: Sequelize.FLOAT,
        discount: Sequelize.FLOAT,
        index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('naboritems', { transaction }),
      queryInterface.dropTable('nabors', { transaction }),
      queryInterface.dropTable('groups', { transaction }),
      queryInterface.dropTable('goods', { transaction })
    ]))
};
