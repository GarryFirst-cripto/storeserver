const dotenv = require('dotenv');

dotenv.config();

const isNet = process.env.DB_USERNAME !== 'postgres';

const env = {
  db: {
    database: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    dialectOptions: isNet ? { ssl: { rejectUnauthorized: false } } : {},
    ssl: isNet,
    logging: false
  }
};

const { database, username, password, host, port, dialect, ssl, dialectOptions, logging } = env.db;

module.exports = {
  database, username, password, host, port, dialect, ssl, dialectOptions, logging
};
