export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('zakazs', 'discount', {
        type: Sequelize.FLOAT
      }, { transaction }),
      queryInterface.addColumn('zakazs', 'srokEnd', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.renameColumn('zakazs', 'srok', 'srokStart', { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.renameColumn('zakazs', 'srokStart', 'srok', { transaction }),
      queryInterface.removeColumn('zakazs', 'srokEnd', { transaction }),
      queryInterface.removeColumn('zakazs', 'discount', { transaction })
    ]))
};
