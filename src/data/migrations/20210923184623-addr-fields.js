export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('addrs', 'latitude', {
        type: Sequelize.FLOAT
      }, { transaction }),
      queryInterface.addColumn('addrs', 'longitude', {
        type: Sequelize.FLOAT
      }, { transaction }),
      queryInterface.addColumn('addrs', 'mailindex', {
        type: Sequelize.INTEGER
      }, { transaction }),
      queryInterface.addColumn('addrs', 'region', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('addrs', 'street', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('addrs', 'house', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('addrs', 'flat', {
        type: Sequelize.INTEGER
      }, { transaction }),
      queryInterface.changeColumn('addrs', 'city', {
        allowNull: true,
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.removeColumn('addrs', 'address', { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('addrs', 'address', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.removeColumn('addrs', 'flat', { transaction }),
      queryInterface.removeColumn('addrs', 'house', { transaction }),
      queryInterface.removeColumn('addrs', 'street', { transaction }),
      queryInterface.removeColumn('addrs', 'region', { transaction }),
      queryInterface.removeColumn('addrs', 'mailindex', { transaction }),
      queryInterface.removeColumn('addrs', 'longitude', { transaction }),
      queryInterface.removeColumn('addrs', 'latitude', { transaction })
    ]))
};
