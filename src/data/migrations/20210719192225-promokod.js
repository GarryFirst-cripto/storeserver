export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.createTable('promokods', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        kodname: Sequelize.STRING,
        mode: Sequelize.INTEGER,
        totalZakaz: Sequelize.FLOAT,
        goodDiscount: Sequelize.FLOAT,
        freeGood: Sequelize.FLOAT,
        goodId: {
          type: Sequelize.UUID,
          references: {
            model: 'goods',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        lastdata: Sequelize.DATE,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('paytypes', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        payname: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        mode: Sequelize.BOOLEAN,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('admins', 'email', {
        type: Sequelize.STRING,
        unique: true
      }, { transaction }),
      queryInterface.addColumn('nabors', 'mode', {
        type: Sequelize.STRING
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('nabors', 'mode', { transaction }),
      queryInterface.removeColumn('admins', 'email', { transaction }),
      queryInterface.dropTable('paytypes', { transaction }),
      queryInterface.dropTable('promokods', { transaction })
    ]))
};
