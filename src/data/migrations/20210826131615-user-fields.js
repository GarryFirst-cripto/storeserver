export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'confirmed', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('nabors', 'hidden', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('nabors', 'keytext', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.removeColumn('naboritems', 'index', { transaction }),
      queryInterface.addColumn('naboritems', 'kolich', {
        type: Sequelize.FLOAT
      }, { transaction }),
      queryInterface.addColumn('zakazs', 'number', {
        type: Sequelize.BIGINT
      }, { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('zakazs', 'number', { transaction }),
      queryInterface.addColumn('naboritems', 'index', {
        type: Sequelize.INTEGER
      }, { transaction }),
      queryInterface.removeColumn('naboritems', 'kolich', { transaction }),
      queryInterface.removeColumn('nabors', 'keytext', { transaction }),
      queryInterface.removeColumn('nabors', 'hidden', { transaction }),
      queryInterface.removeColumn('users', 'confirmed', { transaction })
    ]))
};
