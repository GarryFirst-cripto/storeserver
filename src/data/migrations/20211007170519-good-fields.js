export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'crm_id', {
        type: Sequelize.BIGINT
      }, { transaction }),
      queryInterface.addColumn('goods', 'crm_id', {
        type: Sequelize.BIGINT
      }, { transaction }),
      queryInterface.addColumn('zakazs', 'crm_id', {
        type: Sequelize.BIGINT
      }, { transaction }),
      queryInterface.removeColumn('zakazs', 'pay', { transaction }),
      queryInterface.removeColumn('zakazs', 'address', { transaction }),
      queryInterface.addColumn('pays', 'zakazId', {
        type: Sequelize.UUID,
        references: {
          model: 'zakazs',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('addrs', 'zakazId', {
        type: Sequelize.UUID,
        references: {
          model: 'zakazs',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('addrs', 'korpus', {
        type: Sequelize.STRING(10)
      }, { transaction }),
      queryInterface.addColumn('addrs', 'floor', {
        type: Sequelize.INTEGER
      }, { transaction }),
      queryInterface.createTable('variants', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        goodId: {
          type: Sequelize.UUID,
          references: {
            model: 'goods',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        kolich: Sequelize.FLOAT,
        price: Sequelize.FLOAT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('variants', { transaction }),
      queryInterface.removeColumn('users', 'crm_id', { transaction }),
      queryInterface.removeColumn('goods', 'crm_id', { transaction }),
      queryInterface.removeColumn('zakazs', 'crm_id', { transaction }),
      queryInterface.addColumn('zakazs', 'pay', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('zakazs', 'address', {
        type: Sequelize.TEXT
      }, { transaction }),
      queryInterface.removeColumn('pays', 'zakazId', { transaction }),
      queryInterface.removeColumn('addrs', 'zakazId', { transaction }),
      queryInterface.removeColumn('addrs', 'korpus', { transaction }),
      queryInterface.removeColumn('addrs', 'floor', { transaction })
    ]))
};
