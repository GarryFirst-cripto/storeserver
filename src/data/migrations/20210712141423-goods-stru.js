export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('goods', 'unit', {
        type: Sequelize.STRING(20)
      }, { transaction }),
      queryInterface.addColumn('goods', 'firstdata', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('goods', 'lastdata', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('goods', 'tags', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('goods', 'hidden', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('nabors', 'firstdata', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('nabors', 'lastdata', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('nabors', 'tags', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('groups', 'tags', {
        type: Sequelize.STRING
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('groups', 'tags', { transaction }),
      queryInterface.removeColumn('nabors', 'tags', { transaction }),
      queryInterface.removeColumn('nabors', 'lastdata', { transaction }),
      queryInterface.removeColumn('nabors', 'firstdata', { transaction }),
      queryInterface.removeColumn('goods', 'hidden', { transaction }),
      queryInterface.removeColumn('goods', 'tags', { transaction }),
      queryInterface.removeColumn('goods', 'lastdata', { transaction }),
      queryInterface.removeColumn('goods', 'firstdata', { transaction }),
      queryInterface.removeColumn('goods', 'unit', { transaction })
    ]))
};

