export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('promokods', 'firstdata', {
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('promokods', 'hidden', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('promokods', 'busket', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('groups', 'hidden', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('groups', 'picture', {
        type: Sequelize.STRING
      }, { transaction })

    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('groups', 'picture', { transaction }),
      queryInterface.removeColumn('groups', 'hidden', { transaction }),
      queryInterface.removeColumn('promokods', 'busket', { transaction }),
      queryInterface.removeColumn('promokods', 'hidden', { transaction }),
      queryInterface.removeColumn('promokods', 'firstdata', { transaction })
    ]))
};

