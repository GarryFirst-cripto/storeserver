export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        phone: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        username: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        pushId: Sequelize.STRING,
        status: Sequelize.STRING,
        info: Sequelize.STRING,
        balans: Sequelize.FLOAT,
        bonus: Sequelize.FLOAT,
        verify: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('pays', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        cartName: {
          allowNull: false,
          type: Sequelize.STRING
        },
        number: {
          allowNull: false,
          type: Sequelize.BIGINT
        },
        srok: {
          allowNull: false,
          type: Sequelize.STRING
        },
        cvc: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('addrs', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        city: {
          allowNull: false,
          type: Sequelize.STRING
        },
        address: {
          allowNull: false,
          type: Sequelize.STRING
        },
        dopInfo: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('admins', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        phone: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        username: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        privilage: Sequelize.BOOLEAN,
        verify: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('users', { transaction }),
      queryInterface.dropTable('pays', { transaction }),
      queryInterface.dropTable('addrs', { transaction }),
      queryInterface.dropTable('admins', { transaction })
    ]))
};
