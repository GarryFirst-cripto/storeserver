export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('zakazs', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        adminId: {
          type: Sequelize.UUID,
          references: {
            model: 'admins',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true
        },
        pay: Sequelize.STRING,
        address: Sequelize.TEXT,
        summa: Sequelize.FLOAT,
        srok: Sequelize.DATE,
        info: Sequelize.STRING,
        status: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('zakazitems', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        zakazId: {
          type: Sequelize.UUID,
          references: {
            model: 'zakazs',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true
        },
        goodId: {
          type: Sequelize.UUID,
          references: {
            model: 'goods',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true
        },
        naborId: {
          type: Sequelize.UUID,
          references: {
            model: 'nabors',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true
        },
        kolich: Sequelize.FLOAT,
        price: Sequelize.FLOAT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.changeColumn('goods', 'picture', {
        type: Sequelize.STRING(500)
      }, { transaction }),
      queryInterface.changeColumn('nabors', 'picture', {
        type: Sequelize.STRING(500)
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('zakazitems', { transaction }),
      queryInterface.dropTable('zakazs', { transaction })
    ]))
};
