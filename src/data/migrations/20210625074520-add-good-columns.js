export default {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('naboritems', 'naborId', {
        type: Sequelize.UUID,
        references: {
          model: 'nabors',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('naboritems', 'goodId', {
        type: Sequelize.UUID,
        references: {
          model: 'goods',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('naboritems', 'goodId', { transaction }),
      queryInterface.removeColumn('naboritems', 'naborId', { transaction })
    ]))
};

