/* eslint-disable no-param-reassign */
import { Op } from 'sequelize';
import { ZakazModel, ZakazItemModel, GoodModel, NaborModel, NaborItemModel, UserModel, PayModel, AddressModel } from '../models';
import BaseRepository from './baseRepository';
import asyncForEach from '../../api/helpers/asyncHelper';

class ZakazRepository extends BaseRepository {
  async getList(filter) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { ...rest };
      // eslint-disable-next-line no-console
      const result = await this.model.findAll({
        where,
        order: [['srokStart', 'desc']],
        include: [{
          model: UserModel
        }, {
          model: PayModel
        }, {
          model: AddressModel
        }, {
          model: ZakazItemModel,
          include: [{
            model: GoodModel
          }, {
            model: NaborModel,
            include: {
              model: NaborItemModel,
              include: {
                model: GoodModel
              }
            }
          }]
        }],
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getZakazById(id) {
    const result = await this.model.findOne({
      where: { id },
      include: [{
        model: UserModel
      }, {
        model: PayModel
      }, {
        model: AddressModel
      }, {
        model: ZakazItemModel,
        include: [{
          model: GoodModel
        }, {
          model: NaborModel,
          include: {
            model: NaborItemModel,
            include: {
              model: GoodModel
            }
          }
        }]
      }]
    });
    return result;
  }

  async getByUserId(userId, filter) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { userId, ...rest };
      const result = await this.model.findAll({
        where,
        order: [['srokStart', 'desc']],
        include: [{
          model: UserModel
        }, {
          model: PayModel
        }, {
          model: AddressModel
        }, {
          model: ZakazItemModel,
          include: [{
            model: GoodModel
          }, {
            model: NaborModel,
            include: {
              model: NaborItemModel,
              include: {
                model: GoodModel
              }
            }
          }]
        }],
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async create(data) {
    if (!ZakazRepository.zakazNumber) {
      const item = await this.model.findAll({
        where: { number: { [Op.ne]: null } },
        order: [['number', 'desc']],
        offset: 0,
        limit: 1
      });
      if (item.length === 0) {
        ZakazRepository.zakazNumber = 0;
      } else {
        ZakazRepository.zakazNumber = Number(item[0].number);
      }
    }
    try {
      ZakazRepository.zakazNumber += 1;
      data.number = ZakazRepository.zakazNumber;
      data.createdAt = new Date();
      const zakaz = await this.model.create(data);
      if (data.zakazitems) {
        const { id: zakazId } = zakaz;
        await asyncForEach(async item => {
          item.zakazId = zakazId;
          item.createdAt = new Date();
          await ZakazItemModel.create(item);
        }, data.zakazitems);
      }
      if (data.pay) {
        const { id: zakazId } = zakaz;
        const item = data.pay;
        item.zakazId = zakazId;
        item.createdAt = new Date();
        // eslint-disable-next-line no-console
        // console.log(item);
        await PayModel.create(item);
      }
      if (data.addr) {
        const { id: zakazId } = zakaz;
        const item = data.addr;
        item.zakazId = zakazId;
        item.createdAt = new Date();
        await AddressModel.create(item);
      }
      const result = await this.getZakazById(zakaz.id);
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async updateZakazById(id, data) {
    data.updatedAt = new Date();
    try {
      await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      if (data.zakazitems) {
        await ZakazItemModel.destroy({ where: { zakazId: id } });
        await asyncForEach(async item => {
          item.zakazId = id;
          if (!item.createdAt) item.createdAt = new Date();
          item.updatedAt = new Date();
          await ZakazItemModel.create(item);
        }, data.zakazitems);
      }
      if (data.pay) {
        await PayModel.destroy({ where: { zakazId: id } });
        const item = data.pay;
        item.zakazId = id;
        if (!item.createdAt) item.createdAt = new Date();
        item.updateddAt = new Date();
        await PayModel.create(item);
      }
      if (data.addr) {
        await AddressModel.destroy({ where: { zakazId: id } });
        const item = data.addr;
        item.zakazId = id;
        if (!item.createdAt) item.createdAt = new Date();
        item.updateddAt = new Date();
        await AddressModel.create(item);
      }
      const result = await this.getZakazById(id);
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async delete(id) {
    const lines = await ZakazItemModel.destroy({ where: { zakazId: id } });
    const pays = await PayModel.destroy({ where: { zakazId: id } });
    const address = await AddressModel.destroy({ where: { zakazId: id } });
    const result = await this.model.destroy({ where: { id } });
    return { result, lines, pays, address };
  }
}

export default new ZakazRepository(ZakazModel);
