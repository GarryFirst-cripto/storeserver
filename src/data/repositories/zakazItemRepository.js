import { ZakazModel, ZakazItemModel, GoodModel, NaborModel, NaborItemModel } from '../models';
import BaseRepository from './baseRepository';

class ZakazItemRepository extends BaseRepository {
  async getByGoodId(goodId) {
    const result = await this.model.findAll({
      where: { goodId },
      include: [{
        model: ZakazModel
      }, {
        model: GoodModel
      }]
    });
    return result;
  }

  async getByNaborId(naborId) {
    const result = await this.model.findAll({
      where: { naborId },
      include: [{
        model: ZakazModel
      }, {
        model: NaborModel,
        include: {
          model: NaborItemModel,
          include: {
            model: GoodModel
          }
        }
      }]
    });
    return result;
  }

  async getZakazUserId(id) {
    const result = await this.model.findOne({
      where: { id },
      include: {
        model: ZakazModel
      }
    });
    if (result && result.zakaz) {
      return result.zakaz.dataValues.userId;
    }
    return null;
  }
}

export default new ZakazItemRepository(ZakazItemModel);
