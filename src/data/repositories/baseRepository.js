/* eslint-disable no-param-reassign */
export default class BaseRepository {
  constructor(model) {
    this.model = model;
  }

  async getAll() {
    const result = await this.model.findAll();
    return result;
  }

  async getList(filter) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { ...rest };
      // eslint-disable-next-line no-console
      const result = await this.model.findAll({ where, offset, limit });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getById(id) {
    return this.model.findByPk(id);
  }

  async create(data) {
    data.createdAt = new Date();
    try {
      return await this.model.create(data);
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async updateById(id, data) {
    data.updatedAt = new Date();
    delete data.id;
    try {
      const result = await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      return result[1];
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async delete(id) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }
}
