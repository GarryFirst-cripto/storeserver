import { VariantModel } from '../models';
import BaseRepository from './baseRepository';

class VariantRepository extends BaseRepository {
  async getById(id) {
    const result = await this.model.findOne({
      where: { id }
    });
    return result;
  }

  async deleteGoodVari(goodId) {
    const result = await this.model.destroy({ where: { goodId } });
    return result;
  }
}

export default new VariantRepository(VariantModel);
