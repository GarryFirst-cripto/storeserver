import { NaborItemModel, NaborModel, GoodModel } from '../models';
import BaseRepository from './baseRepository';

class NaborItemRepository extends BaseRepository {
  async getByGoodId(goodId) {
    const result = await this.model.findAll({
      where: { goodId },
      include: [{
        model: GoodModel
      }, {
        model: NaborModel
      }]
    });
    return result;
  }
}

export default new NaborItemRepository(NaborItemModel);
