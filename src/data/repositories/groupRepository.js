import { Op } from 'sequelize';
import { GroupModel } from '../models';
import BaseRepository from './baseRepository';

class GroupRepository extends BaseRepository {
  async getAll() {
    const result = await this.model.findAll({ order: [['index', 'desc'], 'groupname'] });
    return result;
  }

  async getList(filter) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { ...rest };
      const result = await this.model.findAll({
        where,
        order: [['index', 'desc'], 'groupname'],
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getByName(groupname) {
    const result = await this.model.findOne({ where: { groupname } });
    return result;
  }

  async getByTags(id, filter) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const tagList = [];
    arr.forEach(item => {
      tagList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      where: { tags: { [Op.iLike]: { [Op.any]: tagList } } },
      order: [['index', 'desc'], 'groupname'],
      offset,
      limit
    });
    return result;
  }

  async getByText(text, filter) {
    const { from: offset, count: limit } = filter;
    const fltText = `%${text}%`;
    const result = await this.model.findAll({
      where: { [Op.or]: [{ groupname: { [Op.iLike]: fltText } }, { tags: { [Op.iLike]: fltText } }] },
      order: [['index', 'desc'], 'groupname'],
      offset,
      limit
    });
    return result;
  }

  async delete(id) {
    const parentId = id;
    const subGroups = await this.model.findAll({ where: { parentId } });
    subGroups.forEach(async item => {
      const { dataValues: { id: idd } } = item;
      await this.delete(idd);
    });
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }
}

export default new GroupRepository(GroupModel);
