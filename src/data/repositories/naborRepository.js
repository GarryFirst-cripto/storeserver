/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
import { Op } from 'sequelize';
import { NaborModel, NaborItemModel, GoodModel, ZakazModel, UserModel, ZakazItemModel } from '../models';
import BaseRepository from './baseRepository';
import asyncForEach from '../../api/helpers/asyncHelper';

class NaborRepository extends BaseRepository {
  async getByName(naborname) {
    const result = await this.model.findOne({ where: { naborname } });
    if (result) {
      result.dataValues.clientPrice = this.getClientPrice(result);
    }
    return result;
  }

  getClientPrice(nabor) {
    if (nabor.discount) {
      const startTime = nabor.firstdata ? new Date(nabor.firstdata).getTime() : 0;
      const lastTime = nabor.lastdata ? new Date(nabor.lastdata).getTime() : Number.MAX_SAFE_INTEGER;
      const current = new Date().getTime();
      if (current >= startTime && current <= lastTime) {
        const price = nabor.price * (1 - nabor.discount / 100);
        return Math.round(price * 100) / 100;
      }
    }
    return nabor.price;
  }

  async getByGroup(id, filter) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const grList = [];
    arr.forEach(item => {
      grList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      where: { groupp: { [Op.iLike]: { [Op.any]: grList } } },
      order: [['index', 'desc'], 'naborname'],
      offset,
      limit
    });
    result.forEach(item => {
      item.dataValues.clientPrice = this.getClientPrice(item);
    });
    return result;
  }

  async getByTags(id, filter) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const tagList = [];
    arr.forEach(item => {
      tagList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      // where: { tags: { [Sequelize.Op.substring]: id } },
      where: { tags: { [Op.iLike]: { [Op.any]: tagList } } },
      order: [['index', 'desc'], 'naborname'],
      include: [{
        model: NaborItemModel,
        include: {
          model: GoodModel
        }
      }],
      offset,
      limit
    });
    result.forEach(item => {
      item.dataValues.clientPrice = this.getClientPrice(item);
    });
    return result;
  }

  async getList(filter) {
    try {
      const { from: offset, count: limit } = filter;
      delete filter.from;
      delete filter.count;
      const result = await this.model.findAll({ where: filter, offset, limit });
      result.forEach(item => {
        item.dataValues.clientPrice = this.getClientPrice(item);
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getFullList(filter) {
    const { from: offset, count: limit } = filter;
    delete filter.from;
    delete filter.count;
    const result = await this.model.findAll({
      where: filter,
      include: [{
        model: NaborItemModel,
        include: {
          model: GoodModel
        }
      }],
      offset,
      limit
    });
    result.forEach(item => {
      item.dataValues.clientPrice = this.getClientPrice(item);
    });
    return result;
  }

  async getFullZakazList(filter) {
    const { from: offset, count: limit, number, data, dataFrom, dataTo, userId, username } = filter;
    let whereZakaz = {};
    if (data) {
      const dataA = new Date(data);
      dataA.setHours(dataA.getTimezoneOffset() / (-1 * 60), 0, 0);
      const dataB = new Date(dataA);
      dataB.setDate(dataB.getDate() + 1);
      whereZakaz = { [Op.and]: [{ srokStart: { [Op.gte]: dataA } }, { srokStart: { [Op.lt]: dataB } }] };
    } else if (dataFrom && dataTo) {
      const dataA = new Date(dataFrom);
      const dataB = new Date(dataTo);
      whereZakaz = { [Op.and]: [{ srokStart: { [Op.gte]: dataA } }, { srokStart: { [Op.lt]: dataB } }] };
    }
    if (number) whereZakaz.number = number;
    if (userId) whereZakaz.userId = userId;
    const whereUser = username ? { username } : {};
    const result = await this.model.findAll({
      include: [{
        model: ZakazItemModel,
        required: true,
        include: {
          model: ZakazModel,
          where: whereZakaz,
          required: true,
          include: {
            model: UserModel,
            where: whereUser,
            required: true
          }
        }
      }, {
        model: NaborItemModel,
        include: {
          model: GoodModel
        }
      }],
      offset,
      limit
    });
    result.forEach(item => {
      item.dataValues.clientPrice = this.getClientPrice(item);
    });
    return result;
  }

  async getById(id) {
    const result = await this.model.findOne({
      where: { id },
      include: [{
        model: NaborItemModel,
        include: {
          model: GoodModel
        }
      }]
    });
    if (result) {
      result.dataValues.clientPrice = this.getClientPrice(result);
    }
    return result;
  }

  async create(data) {
    data.createdAt = new Date();
    try {
      const nabor = await this.model.create(data);
      if (data.naboritems) {
        const { id: naborId } = nabor;
        await asyncForEach(async item => {
          item.naborId = naborId;
          item.createdAt = new Date();
          await NaborItemModel.create(item);
        }, data.naboritems);
      }
      const result = await this.getById(nabor.id);
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async updateNaborById(id, data) {
    data.updatedAt = new Date();
    try {
      await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      if (data.naboritems) {
        await NaborItemModel.destroy({ where: { naborId: id } });
        await asyncForEach(async item => {
          item.naborId = id;
          if (!item.createdAt) item.createdAt = new Date();
          item.updatedAt = new Date();
          await NaborItemModel.create(item);
        }, data.naboritems);
      }
      const result = await this.getById(id);
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteItem(id) {
    const result = await NaborItemModel.destroy({ where: { id } });
    return { result };
  }

  async delete(id) {
    const lines = await NaborItemModel.destroy({ where: { naborId: id } });
    const result = await this.model.destroy({ where: { id } });
    return { result, lines };
  }
}

export default new NaborRepository(NaborModel);
