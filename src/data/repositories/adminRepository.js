import { AdminModel } from '../models/index';
import BaseRepository from './baseRepository';

class AdminRepository extends BaseRepository {
  async getByPhone(phone) {
    const result = await this.model.findOne({ where: { phone } });
    return result;
  }

  async getByEmail(mail) {
    const email = mail.toLowerCase();
    const result = await this.model.findOne({ where: { email } });
    return result;
  }

  async getByName(username) {
    const result = await this.model.findOne({ where: { username } });
    return result;
  }
}

export default new AdminRepository(AdminModel);
