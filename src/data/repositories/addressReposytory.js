import { AddressModel } from '../models';
import BaseRepository from './baseRepository';

class PaytypeRepository extends BaseRepository {
  async getById(id) {
    const result = await this.model.findOne({
      where: { id }
    });
    return result;
  }
}

export default new PaytypeRepository(AddressModel);
