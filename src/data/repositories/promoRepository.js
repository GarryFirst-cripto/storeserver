import { GoodModel, PromoKodModel } from '../models';
import BaseRepository from './baseRepository';

class PromoRepository extends BaseRepository {
  async getList(filter) {
    try {
      const { from: offset, count: limit } = filter;
      const result = await this.model.findAll({
        where: filter,
        include: {
          model: GoodModel
        },
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getById(id) {
    const result = await this.model.findOne({
      where: { id },
      include: {
        model: GoodModel
      }
    });
    return result;
  }

  async getByName(kodname) {
    const result = await this.model.findOne({
      where: { kodname },
      include: {
        model: GoodModel
      }
    });
    return result;
  }
}

export default new PromoRepository(PromoKodModel);
