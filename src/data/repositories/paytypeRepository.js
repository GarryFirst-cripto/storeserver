import { PaytypeModel } from '../models';
import BaseRepository from './baseRepository';

class PaytypeRepository extends BaseRepository {
  async getList(filter) {
    try {
      const { from: offset, count: limit } = filter;
      const result = await this.model.findAll({
        where: filter,
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getById(id) {
    const result = await this.model.findOne({
      where: { id }
    });
    return result;
  }
}

export default new PaytypeRepository(PaytypeModel);
