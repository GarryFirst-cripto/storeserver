import { Op } from 'sequelize';
import { UserModel, PayModel, AddressModel } from '../models/index';
import BaseRepository from './baseRepository';
import asyncForEach from '../../api/helpers/asyncHelper';

class UserRepository extends BaseRepository {
  async getUserById(userId) {
    return this.model.findOne({
      where: { id: userId },
      include: [{
        model: PayModel,
        required: false,
        where: { userId }
      },
      {
        model: AddressModel,
        required: false,
        where: { userId }
      }]
    });
  }

  async getByText(text, filter) {
    const { from: offset, count: limit } = filter;
    const fltText = `%${text}%`;
    const result = await this.model.findAll({
      where: { [Op.or]: [{ username: { [Op.iLike]: fltText } }, { phone: { [Op.iLike]: fltText } }] },
      order: ['username'],
      offset,
      limit
    });
    return result;
  }

  // eslint-disable-next-line class-methods-use-this
  async updateAddons(model, addons, userId) {
    await model.destroy({ where: { userId } });
    await asyncForEach(async item => {
      // if (item.id) {
      //   const { id } = item;
      //   await model.update(item, {
      //     where: { id },
      //     returning: true,
      //     plain: true
      //   });
      // } else {
      // eslint-disable-next-line no-param-reassign
      item.userId = userId;
      await model.create(item);
      // }
    }, addons);
  }

  async updateById(id, data) {
    // eslint-disable-next-line no-param-reassign
    data.updatedAt = new Date();
    try {
      await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      if (data.pays) await this.updateAddons(PayModel, data.pays, id);
      if (data.addrs) await this.updateAddons(AddressModel, data.addrs, id);
      const result = await this.getUserById(id);
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async delete(id) {
    await PayModel.destroy({ where: { userId: id } });
    await AddressModel.destroy({ where: { userId: id } });
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteUserPay(id) {
    const result = await PayModel.destroy({ where: { id } });
    return result;
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteUserAddress(id) {
    const result = await AddressModel.destroy({ where: { id } });
    return result;
  }

  async getByPhone(phone) {
    try {
      const result = await this.model.findOne({ where: { phone } });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getByName(username) {
    try {
      const result = await this.model.findOne({ where: { username } });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }
}

export default new UserRepository(UserModel);
