/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
import { Op } from 'sequelize';
import { GoodModel, VariantModel, ZakazModel, ZakazItemModel, UserModel } from '../models';
import BaseRepository from './baseRepository';

class GoodRepository extends BaseRepository {
  async getByName(goodname) {
    const result = await this.model.findOne({ where: { goodname } });
    if (result) {
      result.dataValues.clientPrice = this.getClientPrice(result);
    }
    return result;
  }

  async getById(id) {
    const result = await this.model.findOne({
      where: { id },
      include: [{
        model: VariantModel
      }, {
        model: ZakazItemModel,
        include: [{
          model: ZakazModel,
          include: [{
            model: UserModel,
            attributes: ['username']
          }]
        }]
      }]
    });
    if (result) {
      result.dataValues.clientPrice = this.getClientPrice(result);
    }
    return result;
  }

  getClientPrice(good) {
    if (good.discount) {
      const startTime = good.firstdata ? new Date(good.firstdata).getTime() : 0;
      const lastTime = good.lastdata ? new Date(good.lastdata).getTime() : Number.MAX_SAFE_INTEGER;
      const current = new Date().getTime();
      if (current >= startTime && current <= lastTime) {
        const price = good.price * (1 - good.discount / 100);
        return Math.round(price * 100) / 100;
      }
    }
    return good.price;
  }

  getClientIndex(good, userId) {
    let indexClient = 0;
    let indexAll = 0;
    good.zakazitems.forEach(item => {
      indexAll += item.kolich;
      if (item.zakaz.userId === userId) {
        indexClient += item.kolich;
      }
    });
    return { indexClient, indexAll };
  }

  async getList(filter, userId) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { ...rest };
      const result = await this.model.findAll({
        where,
        order: [['index', 'desc'], 'goodname'],
        include: [{
          model: VariantModel
        }, {
          model: ZakazItemModel,
          attributes: ['kolich'],
          include: [{
            model: ZakazModel,
            attributes: ['userId']
          }]
        }],
        offset,
        limit
      });
      result.forEach(item => {
        const { indexClient, indexAll } = this.getClientIndex(item, userId);
        item.dataValues.indexClient = indexClient;
        item.dataValues.indexAll = indexAll;
        item.dataValues.clientPrice = this.getClientPrice(item);
        delete item.dataValues.zakazitems;
      });
      result.sort((itemA, itemB) => {
        let ress = itemB.dataValues.indexClient - itemA.dataValues.indexClient;
        if (ress === 0) ress = itemB.dataValues.indexAll - itemA.dataValues.indexAll;
        if (ress === 0) ress = itemB.dataValues.index - itemA.dataValues.index;
        if (ress === 0) ress = itemA.dataValues.goodname - itemB.dataValues.goodname;
        return ress;
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getByUser(filter, userId) {
    try {
      const { from: offset, count: limit, ...rest } = filter;
      const where = { ...rest };
      const result = await this.model.findAll({
        where,
        order: [['index', 'desc'], 'goodname'],
        include: [{
          model: ZakazItemModel,
          required: true,
          include: [{
            model: VariantModel
          }, {
            model: ZakazModel,
            where: { userId },
            required: true,
            include: [{
              model: UserModel,
              attributes: ['username']
            }]
          }]
        }],
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getFullList(filter) {
    try {
      const { from: offset, count: limit, number, data, dataFrom, dataTo, userId, username, ...rest } = filter;
      const where = { ...rest };
      let whereZakaz = {};
      if (data) {
        const dataA = new Date(data);
        dataA.setHours(dataA.getTimezoneOffset() / (-1 * 60), 0, 0);
        const dataB = new Date(dataA);
        dataB.setDate(dataB.getDate() + 1);
        whereZakaz = { [Op.and]: [{ srokStart: { [Op.gte]: dataA } }, { srokStart: { [Op.lt]: dataB } }] };
      } else if (dataFrom && dataTo) {
        const dataA = new Date(dataFrom);
        const dataB = new Date(dataTo);
        whereZakaz = { [Op.and]: [{ srokStart: { [Op.gte]: dataA } }, { srokStart: { [Op.lt]: dataB } }] };
      }
      if (number) whereZakaz.number = number;
      if (userId) whereZakaz.userId = userId;
      const whereUser = username ? { username } : {};
      const result = await this.model.findAll({
        where,
        order: [['index', 'desc'], 'goodname'],
        include: [{
          model: VariantModel
        }, {
          model: ZakazItemModel,
          required: true,
          include: [{
            model: ZakazModel,
            where: whereZakaz,
            required: true,
            include: [{
              model: UserModel,
              where: whereUser,
              required: true,
              attributes: ['username']
            }]
          }]
        }],
        offset,
        limit
      });
      return result;
    } catch (err) {
      return { status: 400, message: err };
    }
  }

  async getByGroup(id, filter, userId) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const grList = [];
    arr.forEach(item => {
      grList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      where: { groupp: { [Op.iLike]: { [Op.any]: grList } } },
      order: [['index', 'desc'], 'goodname'], // [Sequelize.fn('RANDOM')],
      include: [{
        model: VariantModel
      }, {
        model: ZakazItemModel,
        attributes: ['kolich'],
        include: [{
          model: ZakazModel,
          attributes: ['userId']
        }]
      }],
      offset,
      limit
    });
    result.forEach(item => {
      const { indexClient, indexAll } = this.getClientIndex(item, userId);
      item.dataValues.indexClient = indexClient;
      item.dataValues.indexAll = indexAll;
      item.dataValues.clientPrice = this.getClientPrice(item);
      delete item.dataValues.zakazitems;
    });
    result.sort((itemA, itemB) => {
      let ress = itemB.dataValues.indexClient - itemA.dataValues.indexClient;
      if (ress === 0) ress = itemB.dataValues.indexAll - itemA.dataValues.indexAll;
      if (ress === 0) ress = itemB.dataValues.index - itemA.dataValues.index;
      if (ress === 0) ress = itemA.dataValues.goodname - itemB.dataValues.goodname;
      return ress;
    });
    return result;
  }

  async getByTags(id, filter, userId) {
    const { from: offset, count: limit } = filter;
    const arr = id.replace(' ', '').split(',');
    const tagList = [];
    arr.forEach(item => {
      tagList.push(`%${item}%`);
    });
    const result = await this.model.findAll({
      where: { tags: { [Op.iLike]: { [Op.any]: tagList } } },
      order: [['index', 'desc'], 'goodname'], // [Sequelize.fn('RANDOM')],
      include: [{
        model: VariantModel
      }, {
        model: ZakazItemModel,
        attributes: ['kolich'],
        include: [{
          model: ZakazModel,
          attributes: ['userId']
        }]
      }],
      offset,
      limit
    });
    result.forEach(item => {
      const { indexClient, indexAll } = this.getClientIndex(item, userId);
      item.dataValues.indexClient = indexClient;
      item.dataValues.indexAll = indexAll;
      item.dataValues.clientPrice = this.getClientPrice(item);
      delete item.dataValues.zakazitems;
    });
    result.sort((itemA, itemB) => {
      let ress = itemB.dataValues.indexClient - itemA.dataValues.indexClient;
      if (ress === 0) ress = itemB.dataValues.indexAll - itemA.dataValues.indexAll;
      if (ress === 0) ress = itemB.dataValues.index - itemA.dataValues.index;
      if (ress === 0) ress = itemA.dataValues.goodname - itemB.dataValues.goodname;
      return ress;
    });
    return result;
  }

  async getByText(text, filter, userId) {
    const { from: offset, count: limit } = filter;
    const fltText = `%${text}%`;
    const result = await this.model.findAll({
      where: { [Op.or]: [{ goodname: { [Op.iLike]: fltText } }, { tags: { [Op.iLike]: fltText } }] },
      order: [['index', 'desc'], 'goodname'],
      include: [{
        model: VariantModel
      }, {
        model: ZakazItemModel,
        attributes: ['kolich'],
        include: [{
          model: ZakazModel,
          attributes: ['userId']
        }]
      }],
      offset,
      limit
    });
    result.forEach(item => {
      const { indexClient, indexAll } = this.getClientIndex(item, userId);
      item.dataValues.indexClient = indexClient;
      item.dataValues.indexAll = indexAll;
      item.dataValues.clientPrice = this.getClientPrice(item);
      delete item.dataValues.zakazitems;
    });
    result.sort((itemA, itemB) => {
      let ress = itemB.dataValues.indexClient - itemA.dataValues.indexClient;
      if (ress === 0) ress = itemB.dataValues.indexAll - itemA.dataValues.indexAll;
      if (ress === 0) ress = itemB.dataValues.index - itemA.dataValues.index;
      if (ress === 0) ress = itemA.dataValues.goodname - itemB.dataValues.goodname;
      return ress;
    });
    return result;
  }
}

export default new GoodRepository(GoodModel);
