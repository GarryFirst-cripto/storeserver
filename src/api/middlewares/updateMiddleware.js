import userRepository from '../../data/repositories/userRepository';

export default async (req, res, next) => {
  const { id, username, phone } = req.body;
  if (phone) {
    const userByPhone = await userRepository.getByPhone(phone);
    if (userByPhone && userByPhone.id !== id) {
      return res.status(401).send({ status: 401, message: 'This Phone is already taken.' });
    }
  }
  if (username) {
    const userByName = await userRepository.getByName(username);
    if (userByName && userByName.id !== id) {
      return res.status(401).send({ status: 401, message: 'Username is already taken.' });
    }
  }
  return next();
};
