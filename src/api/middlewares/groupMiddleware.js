import groupRepository from '../../data/repositories/groupRepository';

export default async (req, res, next) => {
  const { groupname, parentId } = req.body;
  if (groupname) {
    const group = await groupRepository.getByName(groupname);
    if (group) {
      return res.status(401).send({ status: 401, message: 'This Group Name has already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Group Name !' });
  }
  if (parentId) {
    const parent = await groupRepository.getById(parentId);
    if (!parent) {
      return res.status(404).send({ status: 404, message: 'Parent group Id incorrect. No such Group !' });
    }
  }
  return next();
};
