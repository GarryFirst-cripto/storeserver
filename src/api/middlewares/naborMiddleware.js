import naborRepository from '../../data/repositories/naborRepository';

export default async (req, res, next) => {
  const { naborname, price } = req.body;
  if (!price) {
    return res.status(404).send({ status: 404, message: 'No Good Price in data !' });
  }
  if (naborname) {
    const nabor = await naborRepository.getByName(naborname);
    if (nabor) {
      return res.status(401).send({ status: 401, message: 'This Nabor Name has already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Nabor Name in data !' });
  }
  return next();
};
