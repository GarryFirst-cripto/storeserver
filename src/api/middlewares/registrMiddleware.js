import userRepository from '../../data/repositories/userRepository';

export default async (req, res, next) => {
  const { username, phone } = req.body;
  if (phone) {
    const userByPhone = await userRepository.getByPhone(phone);
    if (userByPhone) {
      return res.status(401).send({ status: 401, message: 'This Phone is already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Phone number !' });
  }
  const userByName = await userRepository.getByName(username);
  if (userByName) {
    return res.status(401).send({ status: 401, message: 'Username is already taken.' });
  }
  return next();
};
