import adminRepository from '../../data/repositories/adminRepository';

export default async (req, res, next) => {
  const { id, username, phone, email } = req.body;
  if (phone) {
    const userByPhone = await adminRepository.getByPhone(phone);
    if (userByPhone && userByPhone.id !== id) {
      return res.status(401).send({ status: 401, message: 'This Phone is already taken.' });
    }
  }
  if (email) {
    const userByMail = await adminRepository.getByEmail(email);
    if (userByMail && userByMail.id !== id) {
      return res.status(401).send({ status: 401, message: 'This E-mail is already taken.' });
    }
  }
  if (username) {
    const userByName = await adminRepository.getByName(username);
    if (userByName && userByName.id !== id) {
      return res.status(401).send({ status: 401, message: 'Username is already taken.' });
    }
  }
  return next();
};
