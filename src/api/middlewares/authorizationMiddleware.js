import userRepository from '../../data/repositories/userRepository';
import adminRepository from '../../data/repositories/adminRepository';
import { fetchTokenData } from '../helpers/cryptoHelper';

async function jwtMiddleware(req, res, next) {
  const headers = JSON.parse(JSON.stringify(req.headers));
  const { authorization: token } = headers;
  if (token) {
    try {
      const { id, mode, reff } = fetchTokenData(token);
      const user = (mode > 0 || reff > 0)
        ? await adminRepository.getById(id)
        : await userRepository.getById(id);
      if (user) {
        const { privilage } = user;
        req.user = { id, mode, privilage, reff };
        next();
      } else {
        res.status(401).send({ status: 401, message: 'Unauthorized.' });
      }
    } catch (e) {
      res.status(401).send({ status: 401, message: 'Unauthorized.' });
    }
  } else {
    res.status(401).send({ status: 401, message: 'Unauthorized.' });
  }
}

export default routesWhiteList => (req, res, next) => (
  routesWhiteList.some(route => req.path.startsWith(route))
    ? next()
    : jwtMiddleware(req, res, next)
);
