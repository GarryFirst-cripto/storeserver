import goodRepository from '../../data/repositories/goodRepository';

export default async (req, res, next) => {
  const { goodname, price } = req.body;
  if (!price) {
    return res.status(404).send({ status: 404, message: 'No Good Price in data !' });
  }
  if (goodname) {
    const good = await goodRepository.getByName(goodname);
    if (good) {
      return res.status(401).send({ status: 401, message: 'This Good Name has already taken.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No Good Name !' });
  }
  return next();
};
