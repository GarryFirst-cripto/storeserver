import adminRepository from '../../data/repositories/adminRepository';

export default async (req, res, next) => {
  const { username, phone, email } = req.body;
  if (phone) {
    const userByPhone = await adminRepository.getByPhone(phone);
    if (userByPhone) {
      return res.status(401).send({ status: 401, message: 'This Phone is already taken.' });
    }
  }
  if (email) {
    const userByMail = await adminRepository.getByEmail(email);
    if (userByMail) {
      return res.status(401).send({ status: 401, message: 'This E-mail is already taken.' });
    }
  }
  const userByName = await adminRepository.getByName(username);
  if (userByName) {
    return res.status(401).send({ status: 401, message: 'Username is already taken.' });
  }
  return next();
};
