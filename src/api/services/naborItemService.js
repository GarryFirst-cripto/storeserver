import naborItemRepository from '../../data/repositories/naborItemRepository';

export const getByGood = async (req, res) => {
  const { params: { id: goodId } } = req;
  const list = await naborItemRepository.getByGoodId(goodId);
  return res.status(200).send(list);
};

export const createNaborItem = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const result = await naborItemRepository.create(req.body);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const updateNaborItem = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const result = await naborItemRepository.updateById(req.body.id, req.body);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteNaborItem = async (req, res) => {
  const { params: { id } } = req;
  const { user: { mode } } = req;
  if (mode === 1) {
    const result = await naborItemRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};
