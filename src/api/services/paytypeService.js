import paytypeRepository from '../../data/repositories/paytypeRepository';

export const getKodList = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const filter = req.query;
    const result = await paytypeRepository.getList(filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const getKodById = async (req, res) => {
  const { params: { id } } = req;
  const good = await paytypeRepository.getById(id);
  return res.status(200).send(good);
};

export const create = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const prkod = await paytypeRepository.create(req.body);
    return res.status(200).send(prkod);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const update = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { id } = req.body;
    const prkod = await paytypeRepository.updateById(id, req.body);
    return res.status(200).send(prkod);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const deleteKod = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { params: { id } } = req;
    const result = await paytypeRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
