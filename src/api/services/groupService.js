/* eslint-disable no-param-reassign */
import groupRepository from '../../data/repositories/groupRepository';
import * as imageService from './imageService';
import { host, setHost } from '../helpers/addressHelper';
import { insertPicture, removePicture, parce } from '../helpers/arrayHelper';

export const getGroupList = async (req, res) => {
  const filter = req.query;
  const result = await groupRepository.getList(filter);
  return res.status(200).send(result);
};

const createGroupTree = (group, list) => {
  const result = [];
  list.forEach(item => {
    if (item.parentId === group.id) {
      result.push(item.dataValues);
    }
  });
  result.forEach(item => {
    item.groupList = `${group.groupList}, ${item.id}`;
    item.subGroup = createGroupTree(item, list);
  });
  return result;
};

export const getGroupTree = async res => {
  const groups = await groupRepository.getAll();
  const result = [];
  groups.forEach(item => {
    if (!item.parentId) {
      result.push(item.dataValues);
    }
  });
  result.forEach(item => {
    item.groupList = item.id;
    item.subGroup = createGroupTree(item, groups);
  });
  return res.status(200).send(result);
};

export const getGroupByTags = async (req, res) => {
  const { params: { id } } = req;
  const filter = req.query;
  const result = await groupRepository.getByTags(id, filter);
  return res.status(200).send(result);
};

export const getGroupByText = async (req, res) => {
  const { params: { id: text } } = req;
  const filter = req.query;
  const result = await groupRepository.getByText(text, filter);
  return res.status(200).send(result);
};

export const getGroupById = async (req, res) => {
  const { params: { id } } = req;
  const group = await groupRepository.getById(id);
  return res.status(200).send(group);
};

export const create = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const group = await groupRepository.create(req.body);
    return res.status(200).send(group);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const update = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { id } = req.body;
    const group = await groupRepository.updateById(id, req.body);
    return res.status(200).send(group);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteGroup = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { params: { id } } = req;
    const { picture } = await groupRepository.getById(id);
    imageService.deleteImageList(picture);
    const result = await groupRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const postImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { file: { originalname, buffer } } = req;
    const { groupId, index, name } = parce(originalname);
    const good = await groupRepository.getById(groupId);
    if (good) {
      const links = await imageService.uploadFile(buffer, name || 'image.jpg');
      if (links) {
        const { id, link: publicLink } = links;
        const picture = `${host}${id}`;
        const picturesList = insertPicture(good.picture, picture, index);
        const newGood = await groupRepository.updateById(groupId, { picture: picturesList });
        return res.status(200).send({ picture, publicLink, newGood });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such good ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const removeImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { groupId, picture } = req.body;
    const group = await groupRepository.getById(groupId);
    if (group) {
      await imageService.deleteImage(picture);
      const pictureList = removePicture(group.picture, picture);
      const newGood = await groupRepository.updateById(groupId, { picture: pictureList });
      return res.status(200).send({ picture, newGood });
    }
    return res.status(500).send({ status: 500, message: 'No such good ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
