import goodRepository from '../../data/repositories/goodRepository';
import variantRepository from '../../data/repositories/variantRepository';
import zakazItemRepository from '../../data/repositories/zakazItemRepository';
import naborItemRepository from '../../data/repositories/naborItemRepository';
import * as imageService from './imageService';
import { host, setHost } from '../helpers/addressHelper';
import { insertPicture, removePicture, parce } from '../helpers/arrayHelper';
import asyncForEach from '../helpers/asyncHelper';

export const getGoodByUser = async (req, res) => {
  const { params: { userId } } = req;
  const filter = req.query;
  const result = await goodRepository.getByUser(filter, userId);
  return res.status(200).send(result);
};

export const getGoodByGroup = async (req, res) => {
  const { params: { id } } = req;
  const filter = req.query;
  const userId = req.user.id;
  const result = await goodRepository.getByGroup(id, filter, userId);
  return res.status(200).send(result);
};

export const getGoodByTags = async (req, res) => {
  const { params: { id } } = req;
  const filter = req.query;
  const userId = req.user.id;
  const result = await goodRepository.getByTags(id, filter, userId);
  return res.status(200).send(result);
};

export const getGoodByText = async (req, res) => {
  const { params: { id: text } } = req;
  const filter = req.query;
  const userId = req.user.id;
  const result = await goodRepository.getByText(text, filter, userId);
  return res.status(200).send(result);
};

export const getGoodsList = async (req, res) => {
  const filter = req.query;
  const userId = req.user.id;
  const result = await goodRepository.getList(filter, userId);
  return res.status(200).send(result);
};

export const getGoodsFullList = async (req, res) => {
  const filter = req.query;
  const result = await goodRepository.getFullList(filter);
  return res.status(200).send(result);
};

export const getGoodById = async (req, res) => {
  const { params: { id } } = req;
  const good = await goodRepository.getById(id);
  return res.status(200).send(good);
};

export const create = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const good = await goodRepository.create(req.body);
    const { variants } = req.body;
    if (variants) {
      await asyncForEach(async item => {
        // eslint-disable-next-line no-param-reassign
        item.goodId = good.id;
        await variantRepository.create(item);
      }, variants);
    }
    return res.status(200).send(good);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const update = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { id } = req.body;
    const good = await goodRepository.updateById(id, req.body);
    return res.status(200).send(good);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

// export const deleteGood = async (req, res) => {
//   setHost(req.headers.host);
//   const { user: { mode } } = req;
//   if (mode === 1) {
//     const { params: { id } } = req;
//     const { picture } = await goodRepository.getById(id);
//     imageService.deleteImageList(picture);
//     const result = await goodRepository.delete(id);
//     return res.status(200).send(result);
//   }
//   return res.status(403).send({ status: 403, message: 'Access denied ...' });
// };

export const deleteGood = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { params: { id } } = req;
    const zakazItems = await zakazItemRepository.getByGoodId(id);
    if (zakazItems.length === 0) {
      const naborItems = await naborItemRepository.getByGoodId(id);
      if (naborItems.length === 0) {
        const { picture } = await goodRepository.getById(id);
        imageService.deleteImageList(picture);
        const lines = await variantRepository.deleteGoodVari(id);
        const { result } = await goodRepository.delete(id);
        return res.status(200).send({ result, lines });
      }
    }
    return res.status(406).send({ status: 406, message: 'Forbidden. Good in use ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const postImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { file: { originalname, buffer } } = req;
    const { goodId, index, name } = parce(originalname);
    const good = await goodRepository.getById(goodId);
    if (good) {
      const links = await imageService.uploadFile(buffer, name || 'image.jpg');
      if (links) {
        const { id, link: publicLink } = links;
        const picture = `${host}${id}`;
        const picturesList = insertPicture(good.picture, picture, index);
        const newGood = await goodRepository.updateById(goodId, { picture: picturesList });
        return res.status(200).send({ picture, publicLink, newGood });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such good ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const removeImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { goodId, picture } = req.body;
    const good = await goodRepository.getById(goodId);
    if (good) {
      await imageService.deleteImage(picture);
      const pictureList = removePicture(good.picture, picture);
      const newGood = await goodRepository.updateById(goodId, { picture: pictureList });
      return res.status(200).send({ picture, newGood });
    }
    return res.status(500).send({ status: 500, message: 'No such good ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
