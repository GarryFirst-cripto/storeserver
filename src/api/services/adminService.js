import adminRepository from '../../data/repositories/adminRepository';
import { createAccessToken, createRefreshToken } from '../helpers/cryptoHelper';
import { env } from '../../config/dbConfig';
import sendSMS from '../helpers/smsHelper';
import sendEmail from '../helpers/emailHelper';

export const refresh = async (req, res) => {
  const { user: { id, reff } } = req;
  if (reff) {
    const mode = reff;
    const accessToken = createAccessToken({ id, mode });
    return res.status(200).send({ accessToken });
  }
  return res.status(403).send({ status: 403, message: 'Incorrect token. Try again ...' });
};

export const getAdmins = async (req, res) => {
  if (req.user.mode === 2) {
    const filter = req.query;
    const result = await adminRepository.getList(filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const login = async (req, res) => {
  const { user: { id: userId } } = req;
  const user = await adminRepository.getById(userId);
  if (user) {
    return res.status(200).send({ user });
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const getAdmin = async (req, res) => {
  const { params: { id } } = req;
  const user = await adminRepository.getById(id);
  return res.status(200).send({ user });
};

export const register = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 2) {
    const user = await adminRepository.create(req.body);
    return res.status(200).send(user);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const hostLogin = async (req, res) => {
  const { body: { pwd } } = req;
  const { adminName, adminPWD } = env.admin;
  if (pwd === adminPWD) {
    let admin = await adminRepository.getByName(adminName);
    if (!admin) {
      admin = await adminRepository.create({ phone: '+0000000000', username: adminName, privilage: true });
    }
    const { id } = admin;
    const mode = 2;
    const accessToken = createAccessToken({ id, mode });
    const refreshToken = createRefreshToken({ id, mode });
    return res.status(200).send({ accessToken, refreshToken, user: admin });
  }
  return res.status(403).send({ status: 403, message: 'Incorrect admin passwordcode. Try again ...' });
};

export const signin = async (req, res) => {
  const { phone, kode, repeat } = req.body;
  const user = await adminRepository.getByPhone(phone);
  if (user) {
    if (kode) {
      if (user.verify === kode) {
        const { id } = user;
        const mode = 1;
        const accessToken = createAccessToken({ id, mode });
        const refreshToken = createRefreshToken({ id, mode });
        await adminRepository.updateById(user.id, { verify: '' });
        return res.status(200).send({ accessToken, refreshToken, user });
      }
      return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
    }
    // eslint-disable-next-line no-shadow
    const vKode = repeat ? user.verify : Math.round(1000 + 9000 * Math.random()).toString();
    await adminRepository.updateById(user.id, { verify: vKode });
    const result = await sendSMS(phone, vKode);
    return res.status(200).send({ status: 200, result });
  }
  return res.status(404).send({ status: 404, message: 'No such phone ...' });
};

export const signinEmail = async (req, res) => {
  const { email, kode, repeat } = req.body;
  const user = await adminRepository.getByEmail(email);
  if (user) {
    if (kode) {
      if (user.verify === kode) {
        const { id } = user;
        const mode = 1;
        const accessToken = createAccessToken({ id, mode });
        const refreshToken = createRefreshToken({ id, mode });
        await adminRepository.updateById(user.id, { verify: '' });
        return res.status(200).send({ accessToken, refreshToken, user });
      }
      return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
    }
    // eslint-disable-next-line no-shadow
    const vKode = repeat ? user.verify : Math.round(1000 + 9000 * Math.random()).toString();
    await adminRepository.updateById(user.id, { verify: vKode });
    const result = await sendEmail(email, vKode);
    return res.status(200).send({ status: 200, result });
  }
  return res.status(404).send({ status: 404, message: 'No such E-mail ...' });
};

export const updateAdmin = async (req, res) => {
  const { body: { id: userId } } = req;
  if (req.user.mode === 2 || req.user.id === userId) {
    const result = await adminRepository.updateById(userId, req.body);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteAdmin = async (req, res) => {
  if (req.user.mode === 2) {
    const { params: { id } } = req;
    const result = await adminRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};
