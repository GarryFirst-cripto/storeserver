import variantRepository from '../../data/repositories/variantRepository';

export const getVariByGoodId = async (req, res) => {
  const { params: { goodId } } = req;
  const result = await variantRepository.getList({ goodId });
  return res.status(200).send(result);
};

export const getVariById = async (req, res) => {
  const { params: { id } } = req;
  const result = await variantRepository.getById(id);
  return res.status(200).send(result);
};

export const create = async (req, res) => {
  const result = await variantRepository.create(req.body);
  return res.status(200).send(result);
};

export const update = async (req, res) => {
  const { id } = req.body;
  const result = await variantRepository.updateById(id, req.body);
  return res.status(200).send(result);
};

export const deleteVari = async (req, res) => {
  const { params: { id } } = req;
  const result = await variantRepository.delete(id);
  return res.status(200).send(result);
};
