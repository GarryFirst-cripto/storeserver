import addressRepository from '../../data/repositories/addressReposytory';

export const getAddressByUserId = async (req, res) => {
  const { params: { userId } } = req;
  const result = await addressRepository.getList({ userId });
  return res.status(200).send(result);
};

export const getAddressById = async (req, res) => {
  const { params: { id } } = req;
  const address = await addressRepository.getById(id);
  return res.status(200).send(address);
};

export const create = async (req, res) => {
  const address = await addressRepository.create(req.body);
  return res.status(200).send(address);
};

export const update = async (req, res) => {
  const { id } = req.body;
  const address = await addressRepository.updateById(id, req.body);
  return res.status(200).send(address);
};

export const deleteAddress = async (req, res) => {
  const { params: { id } } = req;
  const result = await addressRepository.delete(id);
  return res.status(200).send(result);
};
