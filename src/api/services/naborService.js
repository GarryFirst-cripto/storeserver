import naborRepository from '../../data/repositories/naborRepository';
import * as imageService from './imageService';
import { host, setHost } from '../helpers/addressHelper';
import { insertPicture, removePicture, parce } from '../helpers/arrayHelper';

export const getNaborByGroup = async (req, res) => {
  const { params: { id } } = req;
  const filter = req.query;
  const result = await naborRepository.getByGroup(id, filter);
  return res.status(200).send(result);
};

export const getNaborByTags = async (req, res) => {
  const { params: { id } } = req;
  const filter = req.query;
  const result = await naborRepository.getByTags(id, filter);
  return res.status(200).send(result);
};

export const getNaborsList = async (req, res) => {
  const filter = req.query;
  const result = await naborRepository.getList(filter);
  return res.status(200).send(result);
};

export const getNaborsFullList = async (req, res) => {
  const filter = req.query;
  const result = await naborRepository.getFullList(filter);
  return res.status(200).send(result);
};

export const getNaborsFullZakazList = async (req, res) => {
  const filter = req.query;
  const result = await naborRepository.getFullZakazList(filter);
  return res.status(200).send(result);
};

export const getNaborById = async (req, res) => {
  const { params: { id } } = req;
  const nabor = await naborRepository.getById(id);
  return res.status(200).send(nabor);
};

export const create = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const group = await naborRepository.create(req.body);
    return res.status(200).send(group);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const updateNabor = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { id } = req.body;
    const group = await naborRepository.updateNaborById(id, req.body);
    return res.status(200).send(group);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const updateHeader = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const { id } = req.body;
    const group = await naborRepository.updateById(id, req.body);
    return res.status(200).send(group);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteNabor = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { params: { id } } = req;
    const { picture } = await naborRepository.getById(id);
    imageService.deleteImageList(picture);
    const result = await naborRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const postImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { file: { originalname, buffer } } = req;
    const { naborId, index, name } = parce(originalname);
    const nabor = await naborRepository.getById(naborId);
    if (nabor) {
      const links = await imageService.uploadFile(buffer, name);
      if (links) {
        const { id, link: publicLink } = links;
        const picture = `${host}${id}`;
        const picturesList = insertPicture(nabor.picture, picture, index);
        const newNabor = await naborRepository.updateById(naborId, { picture: picturesList });
        return res.status(200).send({ picture, publicLink, newNabor });
      }
      return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
    }
    return res.status(500).send({ status: 500, message: 'No such nabor ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const removeImage = async (req, res) => {
  setHost(req.headers.host);
  const { user: { mode } } = req;
  if (mode === 1) {
    const { naborId, picture } = req.body;
    const nabor = await naborRepository.getById(naborId);
    if (nabor) {
      await imageService.deleteImage(picture);
      const pictureList = removePicture(nabor.picture, picture);
      const newNabor = await naborRepository.updateById(naborId, { picture: pictureList });
      return res.status(200).send({ picture, newNabor });
    }
    return res.status(500).send({ status: 500, message: 'No such nabor ...' });
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};
