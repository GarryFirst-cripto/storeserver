import zakazItemRepository from '../../data/repositories/zakazItemRepository';
import zakazRepository from '../../data/repositories/zakazRepository';

export const getByGood = async (req, res) => {
  const { params: { id: goodId } } = req;
  const list = await zakazItemRepository.getByGoodId(goodId);
  return res.status(200).send(list);
};

export const getByNabor = async (req, res) => {
  const { params: { id: naborId } } = req;
  const list = await zakazItemRepository.getByNaborId(naborId);
  return res.status(200).send(list);
};

export const createZakazItem = async (req, res) => {
  const { body: { zakazId } } = req;
  const zakazItem = await zakazRepository.getById(zakazId);
  if (zakazItem) {
    const { userId: zakazUserId } = zakazItem;
    const { user: { id: userId, mode } } = req;
    if (mode === 1 || zakazUserId === userId) {
      const result = await zakazItemRepository.create(req.body);
      return res.status(200).send(result);
    }
    return res.status(404).send({ status: 403, message: 'Access denied ...' });
  }
  return res.status(200).send({ result: 0 });
};

export const updateZakazItem = async (req, res) => {
  const { params: { id } } = req;
  const zakazItem = await zakazItemRepository.getZakazUserId(id);
  if (zakazItem) {
    const { userId: zakazUserId } = zakazItem;
    const { user: { id: userId, mode } } = req;
    if (mode === 1 || zakazUserId === userId) {
      const result = await zakazItemRepository.updateById(id, req.body);
      return res.status(200).send(result);
    }
    return res.status(404).send({ status: 403, message: 'Access denied ...' });
  }
  return res.status(200).send({ result: 0 });
};

export const deleteZakazItem = async (req, res) => {
  const { params: { id } } = req;
  const zakazUserId = await zakazItemRepository.getZakazUserId(id);
  if (zakazUserId) {
    const { user: { id: userId, mode } } = req;
    if (mode === 1 || zakazUserId === userId) {
      const result = await zakazItemRepository.delete(id);
      return res.status(200).send(result);
    }
    return res.status(404).send({ status: 403, message: 'Access denied ...' });
  }
  return res.status(200).send({ result: 0 });
};
