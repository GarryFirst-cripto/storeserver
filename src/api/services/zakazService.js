import axios from 'axios';
import zakazRepository from '../../data/repositories/zakazRepository';
import userRepository from '../../data/repositories/userRepository';
import { StrWinToUtf8 } from '../helpers/utfHelper';

const deliveryHttp = 'http://delivery.vod.qsolution.ru:7075/DeliveryRules/GetDeliveryInfo';

export const getZakazList = async (req, res) => {
  const { user: { mode } } = req;
  if (mode === 1) {
    const filter = req.query;
    const result = await zakazRepository.getList(filter);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const getZakazByUser = async (req, res) => {
  const { params: { id: userId }, user: { id, mode } } = req;
  if (mode === 1 || userId === id) {
    const filter = req.query;
    const result = await zakazRepository.getByUserId(userId, filter);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const getZakazById = async (req, res) => {
  const { params: { id } } = req;
  const zakaz = await zakazRepository.getZakazById(id);
  return res.status(200).send(zakaz);
};

export const getZakazDelivery = async (req, res) => {
  const { query: { latitude, longitude } } = req;
  const uri = `${deliveryHttp}?latitude=${latitude}&longitude=${longitude}`;
  const { data } = await axios({
    url: uri,
    method: 'get'
  });
  return res.status(200).send(data);
};

export const getZakazSend = async (req, res) => {
  const { params: { id } } = req;
  const zakaz = await zakazRepository.getZakazById(id);
  if (!zakaz.addr) {
    return res.status(401).send({ status: 401, message: 'Отсутствует адрес доставки в заказе ...' });
  }
  let { crm_id: crmDealId } = zakaz;
  let { user: { crm_id: crmUserId } } = zakaz;
  if (!crmUserId || crmUserId === 0) {
    const data = {
      fields: {
        NAME: StrWinToUtf8(zakaz.user.username),
        OPENED: 'Y',
        ASSIGNED_BY_ID: 19906,
        TYPE_ID: 'CLIENT',
        SOURCE_ID: 'SELF',
        PHONE: [{ VALUE: zakaz.user.phone, VALUE_TYPE: 'WORK' }]
      },
      params: { REGISTER_SONET_EVENT: 'Y' }
    };
    const { data: client } = await axios({
      method: 'post',
      url: 'https://vodovoz.bitrix24.ru/rest/19906/roopky777iknv7oc/crm.contact.add.json',
      data
    });
    const { result } = client;
    if (result && result !== '') {
      crmUserId = result;
      const { user: { id: userId } } = zakaz;
      await userRepository.updateById(userId, { crm_id: crmUserId });
    }
  }
  if (crmUserId) {
    if (!crmDealId) {
      const data = {
        fields: {
          TITLE: StrWinToUtf8(`Заказ ${zakaz.number}`),
          TYPE_ID: 'GOODS',
          STAGE_ID: 'NEW',
          STATUS_ID: 'NEW',
          CONTACT_ID: crmUserId,
          OPENED: 'Y',
          ASSIGNED_BY_ID: 19906,
          PROBABILITY: 30,
          CURRENCY_ID: 'RUB',
          OPPORTUNITY: zakaz.summa,
          UF_CRM_1611649517604: `${zakaz.addr.dataValues.latitude},${zakaz.addr.dataValues.longitude}`,
          UF_CRM_1631791096129: zakaz.addr.dataValues.mailindex,
          UF_CRM_1619785762330: StrWinToUtf8(zakaz.addr.dataValues.region),
          UF_CRM_1619785785877: StrWinToUtf8(zakaz.addr.dataValues.city),
          UF_CRM_1619785797764: StrWinToUtf8(zakaz.addr.dataValues.street),
          UF_CRM_1619785812010: StrWinToUtf8(zakaz.addr.dataValues.house),
          UF_CRM_1631791937045: StrWinToUtf8(zakaz.addr.dataValues.korpus),
          UF_CRM_1631791980053: zakaz.addr.dataValues.floor,
          COMMENTS: StrWinToUtf8(zakaz.info)
        },
        params: { REGISTER_SONET_EVENT: 'Y' }
      };
      const { data: deal } = await axios({
        method: 'post',
        url: 'https://vodovoz.bitrix24.ru/rest/19906/6ky9j9eqshnbgoez/crm.deal.add.json',
        data
      });
      const { result } = deal;
      if (result && result !== '') {
        crmDealId = result;
        await zakazRepository.updateById(id, { crm_id: crmDealId });
      }
    }
    const products = [];
    zakaz.zakazitems.forEach(item => {
      if (item.dataValues.good) {
        products.push({
          PRODUCT_ID: item.dataValues.good.crm_id,
          PRICE: item.dataValues.price,
          QUANTITY: item.dataValues.kolich
        });
      } else if (item.dataValues.nabor) {
        const koe = item.dataValues.kolich;
        item.dataValues.nabor.dataValues.naboritems.forEach(subitem => {
          products.push({
            PRODUCT_ID: subitem.dataValues.good.crm_id,
            PRICE: subitem.dataValues.price,
            QUANTITY: subitem.dataValues.kolich * koe
          });
        });
      }
    });
    const { data: items } = await axios({
      method: 'post',
      url: 'https://vodovoz.bitrix24.ru/rest/19906/s7z1c6ufqmym5nkw/crm.deal.productrows.set.json',
      data: {
        id: crmDealId,
        rows: products
      }
    });
    const { result } = items;
    return res.status(200).send({ result });
  }
  return res.status(500).send({ message: 'Ошибка сервисов crm ...' });
};

export const createZakaz = async (req, res) => {
  const zakaz = await zakazRepository.create(req.body);
  return res.status(200).send(zakaz);
};

export const updateZakaz = async (req, res) => {
  const { user: { id: userId, mode } } = req;
  const { body: { userId: zakazUserId } } = req;
  if (mode === 1 || zakazUserId === userId) {
    const { body: { id } } = req;
    const zakaz = await zakazRepository.updateZakazById(id, req.body);
    return res.status(200).send(zakaz);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const updateHeader = async (req, res) => {
  const { user: { id: userId, mode } } = req;
  const { body: { userId: zakazUserId } } = req;
  if (mode === 1 || zakazUserId === userId) {
    const { body: { id } } = req;
    const zakaz = await zakazRepository.updateById(id, req.body);
    return res.status(200).send(zakaz);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const deleteZakaz = async (req, res) => {
  const { params: { id } } = req;
  const zakaz = await zakazRepository.getById(id);
  if (zakaz) {
    const { userId: zakazUserId } = zakaz;
    const { user: { id: userId, mode } } = req;
    if (mode === 1 || zakazUserId === userId) {
      const result = await zakazRepository.delete(id);
      return res.status(200).send(result);
    }
    return res.status(404).send({ status: 403, message: 'Access denied ...' });
  }
  return res.status(200).send({ result: 0 });
};
