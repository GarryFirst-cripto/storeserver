import paysRepository from '../../data/repositories/paysRepository';

export const getPaysByUserId = async (req, res) => {
  const { params: { userId } } = req;
  const result = await paysRepository.getList({ userId });
  return res.status(200).send(result);
};

export const getPayById = async (req, res) => {
  const { params: { id } } = req;
  const pay = await paysRepository.getById(id);
  return res.status(200).send(pay);
};

export const create = async (req, res) => {
  const pay = await paysRepository.create(req.body);
  return res.status(200).send(pay);
};

export const update = async (req, res) => {
  const { id } = req.body;
  const pay = await paysRepository.updateById(id, req.body);
  return res.status(200).send(pay);
};

export const deletePay = async (req, res) => {
  const { params: { id } } = req;
  const result = await paysRepository.delete(id);
  return res.status(200).send(result);
};
