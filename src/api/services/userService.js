import userRepository from '../../data/repositories/userRepository';
import { createAccessToken, createRefreshToken } from '../helpers/cryptoHelper';
import sendSMS from '../helpers/smsHelper';

export const refresh = async (req, res) => {
  const { user: { id, reff } } = req;
  if (reff) {
    const mode = reff;
    const accessToken = createAccessToken({ id, mode });
    return res.status(200).send({ accessToken });
  }
  return res.status(403).send({ status: 403, message: 'Incorrect token. Try again ...' });
};

export const getUsers = async (req, res) => {
  if (req.user.mode === 1) {
    const filter = req.query;
    const result = await userRepository.getList(filter);
    return res.status(200).send(result);
  }
  return res.status(403).send({ status: 403, message: 'Access denied ...' });
};

export const login = async (req, res) => {
  const { user: { id: userId } } = req;
  const user = await userRepository.getById(userId);
  if (user) {
    return res.status(200).send({ user });
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const getUser = async (req, res) => {
  const { params: { id } } = req;
  const user = await userRepository.getUserById(id);
  return res.status(200).send({ user });
};

export const getUserByText = async (req, res) => {
  const { params: { id: text } } = req;
  const filter = req.query;
  const result = await userRepository.getByText(text, filter);
  return res.status(200).send(result);
};

export const register = async (req, res) => {
  req.body.confirmed = false;
  const user = await userRepository.create(req.body);
  const { id } = user;
  if (id) {
    const { phone } = req.body;
    const vKode = Math.round(1000 + 9000 * Math.random()).toString();
    await userRepository.updateById(id, { verify: vKode });
    const result = await sendSMS(phone, vKode);
    const mode = 0;
    const accessToken = createAccessToken({ id, mode });
    const refreshToken = createRefreshToken({ id, mode });
    return res.status(200).send({ accessToken, refreshToken, user, result, kode: vKode });
  }
  return res.status(500).send(user);
};

export const signin = async (req, res) => {
  const { phone, kode, repeat } = req.body;
  const user = await userRepository.getByPhone(phone);
  if (user) {
    if (kode) {
      if (user.verify === kode) {
        const { id } = user;
        const mode = 0;
        const accessToken = createAccessToken({ id, mode });
        const refreshToken = createRefreshToken({ id, mode });
        await userRepository.updateById(user.id, { verify: '', confirmed: true });
        user.confirmed = true;
        return res.status(200).send({ accessToken, refreshToken, user });
      }
      return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
    }
    // eslint-disable-next-line no-shadow
    const vKode = repeat ? user.verify : Math.round(1000 + 9000 * Math.random()).toString();
    await userRepository.updateById(user.id, { verify: vKode });
    const result = await sendSMS(phone, vKode);
    return res.status(200).send({ status: 200, result, kode: vKode });
  }
  return res.status(404).send({ status: 404, message: 'No such phone ...' });
};

export const updateUser = async (req, res) => {
  const { body: { id: userId } } = req;
  if (req.user.mode === 1 || req.user.id === userId) {
    const result = await userRepository.updateById(userId, req.body);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteUser = async (req, res) => {
  const { params: { id } } = req;
  if (req.user.mode === 1 || id === req.user.id) {
    const result = await userRepository.delete(id);
    return res.status(200).send(result);
  }
  return res.status(404).send({ status: 403, message: 'Access denied ...' });
};

export const deleteUserPay = async (req, res) => {
  const { params: { id } } = req;
  const result = await userRepository.deleteUserPay(id);
  return res.status(200).send(result);
};

export const deleteUserAddress = async (req, res) => {
  const { params: { id } } = req;
  const result = await userRepository.deleteUserAddress(id);
  return res.status(200).send(result);
};
