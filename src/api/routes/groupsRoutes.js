import { Router } from 'express';
import * as groupService from '../services/groupService';
import groupMiddleware from '../middlewares/groupMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/tree', (_req, res, next) => groupService.getGroupTree(res)
    .catch(next))
  .get('/tags/:id', (req, res, next) => groupService.getGroupByTags(req, res)
    .catch(next))
  .get('/find/:id', (req, res, next) => groupService.getGroupByText(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => groupService.getGroupById(req, res)
    .catch(next))
  .get('/', (req, res, next) => groupService.getGroupList(req, res)
    .catch(next))
  .post('/image', fileMiddleware, (req, res, next) => groupService.postImage(req, res)
    .catch(next))
  .post('/destroyimage', (req, res, next) => groupService.removeImage(req, res)
    .catch(next))
  .post('/', groupMiddleware, (req, res, next) => groupService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => groupService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => groupService.deleteGroup(req, res)
    .catch(next));

export default router;
