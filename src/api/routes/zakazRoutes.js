import { Router } from 'express';
import * as zakazService from '../services/zakazService';

const router = Router();

router
  .get('/user/:id', (req, res, next) => zakazService.getZakazByUser(req, res)
    .catch(next))
  .get('/delivery', (req, res, next) => zakazService.getZakazDelivery(req, res)
    .catch(next))
  .get('/send/:id', (req, res, next) => zakazService.getZakazSend(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => zakazService.getZakazById(req, res)
    .catch(next))
  .get('/', (req, res, next) => zakazService.getZakazList(req, res)
    .catch(next))
  .post('/', (req, res, next) => zakazService.createZakaz(req, res)
    .catch(next))
  .put('/header', (req, res, next) => zakazService.updateHeader(req, res)
    .catch(next))
  .put('/', (req, res, next) => zakazService.updateZakaz(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => zakazService.deleteZakaz(req, res)
    .catch(next));

export default router;
