import { Router } from 'express';
import * as variService from '../services/variantService';

const router = Router();

router
  .get('/good/:goodId', (req, res, next) => variService.getVariByGoodId(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => variService.getVariById(req, res)
    .catch(next))
  .post('/', (req, res, next) => variService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => variService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => variService.deleteVari(req, res)
    .catch(next));

export default router;
