import { Router } from 'express';
import * as addressService from '../services/addressService';

const router = Router();

router
  .get('/user/:userId', (req, res, next) => addressService.getAddressByUserId(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => addressService.getAddressById(req, res)
    .catch(next))
  .post('/', (req, res, next) => addressService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => addressService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => addressService.deleteAddress(req, res)
    .catch(next));

export default router;
