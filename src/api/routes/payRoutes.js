import { Router } from 'express';
import * as paysService from '../services/paysService';

const router = Router();

router
  .get('/user/:userId', (req, res, next) => paysService.getPaysByUserId(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => paysService.getPayById(req, res)
    .catch(next))
  .post('/', (req, res, next) => paysService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => paysService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => paysService.deletePay(req, res)
    .catch(next));

export default router;
