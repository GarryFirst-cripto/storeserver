import { Router } from 'express';
import * as paytypeService from '../services/paytypeService';

const router = Router();

router
  .get('/:id', (req, res, next) => paytypeService.getKodById(req, res)
    .catch(next))
  .get('/', (req, res, next) => paytypeService.getKodList(req, res)
    .catch(next))
  .post('/', (req, res, next) => paytypeService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => paytypeService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => paytypeService.deleteKod(req, res)
    .catch(next));

export default router;
