import { Router } from 'express';
import * as promoKodService from '../services/promoKodService';

const router = Router();

router
  .get('/byname/:name', (req, res, next) => promoKodService.getKodByName(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => promoKodService.getKodById(req, res)
    .catch(next))
  .get('/', (req, res, next) => promoKodService.getKodList(req, res)
    .catch(next))
  .post('/', (req, res, next) => promoKodService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => promoKodService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => promoKodService.deleteKod(req, res)
    .catch(next));

export default router;
