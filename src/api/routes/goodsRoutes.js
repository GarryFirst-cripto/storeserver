import { Router } from 'express';
import * as goodService from '../services/goodService';
import goodMiddleware from '../middlewares/goodMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/byuser/:userId', (req, res, next) => goodService.getGoodByUser(req, res)
    .catch(next))
  .get('/group/:id', (req, res, next) => goodService.getGoodByGroup(req, res)
    .catch(next))
  .get('/tags/:id', (req, res, next) => goodService.getGoodByTags(req, res)
    .catch(next))
  .get('/find/:id', (req, res, next) => goodService.getGoodByText(req, res)
    .catch(next))
  .get('/salelist', (req, res, next) => goodService.getGoodsFullList(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => goodService.getGoodById(req, res)
    .catch(next))
  .get('/', (req, res, next) => goodService.getGoodsList(req, res)
    .catch(next))
  .post('/image', fileMiddleware, (req, res, next) => goodService.postImage(req, res)
    .catch(next))
  .post('/destroyimage', (req, res, next) => goodService.removeImage(req, res)
    .catch(next))
  .post('/', goodMiddleware, (req, res, next) => goodService.create(req, res)
    .catch(next))
  .put('/', (req, res, next) => goodService.update(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => goodService.deleteGood(req, res)
    .catch(next));

export default router;
