import { Router } from 'express';
import * as zakazItemService from '../services/zakazItemService';

const router = Router();

router
  .get('/bygood/:id', (req, res, next) => zakazItemService.getByGood(req, res)
    .catch(next))
  .get('/bynabor/:id', (req, res, next) => zakazItemService.getByNabor(req, res)
    .catch(next))
  .post('/', (req, res, next) => zakazItemService.createZakazItem(req, res)
    .catch(next))
  .put('/', (req, res, next) => zakazItemService.updateZakazItem(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => zakazItemService.deleteZakazItem(req, res)
    .catch(next));

export default router;
