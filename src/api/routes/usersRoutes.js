import { Router } from 'express';
import * as userService from '../services/userService';
import registrMiddleware from '../middlewares/registrMiddleware';
import updateMiddleware from '../middlewares/updateMiddleware';

const router = Router();

router
  .get('/refresh', (req, res, next) => userService.refresh(req, res)
    .catch(next))
  .get('/login', (req, res, next) => userService.login(req, res)
    .catch(next))
  .get('/find/:id', (req, res, next) => userService.getUserByText(req, res)
    .catch(next))
  .get('/user/:id', (req, res, next) => userService.getUser(req, res)
    .catch(next))
  .get('/', (req, res, next) => userService.getUsers(req, res)
    .catch(next))
  .post('/register', registrMiddleware, (req, res, next) => userService.register(req, res)
    .catch(next))
  .post('/signin', (req, res, next) => userService.signin(req, res)
    .catch(next))
  .put('/', updateMiddleware, (req, res, next) => userService.updateUser(req, res)
    .catch(next))
  .delete('/pay/:id', (req, res, next) => userService.deleteUserPay(req, res)
    .catch(next))
  .delete('/address/:id', (req, res, next) => userService.deleteUserAddress(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => userService.deleteUser(req, res)
    .catch(next));

export default router;
