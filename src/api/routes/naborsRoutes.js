import { Router } from 'express';
import * as naborService from '../services/naborService';
import naborMiddleware from '../middlewares/naborMiddleware';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/nabors', (req, res, next) => naborService.getNaborsFullList(req, res)
    .catch(next))
  .get('/salelist', (req, res, next) => naborService.getNaborsFullZakazList(req, res)
    .catch(next))
  .get('/group/:id', (req, res, next) => naborService.getNaborByGroup(req, res)
    .catch(next))
  .get('/tags/:id', (req, res, next) => naborService.getNaborByTags(req, res)
    .catch(next))
  .get('/:id', (req, res, next) => naborService.getNaborById(req, res)
    .catch(next))
  .get('/', (req, res, next) => naborService.getNaborsList(req, res)
    .catch(next))
  .post('/image', fileMiddleware, (req, res, next) => naborService.postImage(req, res)
    .catch(next))
  .post('/destroyimage', (req, res, next) => naborService.removeImage(req, res)
    .catch(next))
  .post('/', naborMiddleware, (req, res, next) => naborService.create(req, res)
    .catch(next))
  .put('/header', (req, res, next) => naborService.updateHeader(req, res)
    .catch(next))
  .put('/', (req, res, next) => naborService.updateNabor(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => naborService.deleteNabor(req, res)
    .catch(next));

export default router;
