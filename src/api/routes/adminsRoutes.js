import { Router } from 'express';
import * as adminService from '../services/adminService';
import registrMiddleware from '../middlewares/registrAdminMiddleware';
import updateAdminMiddleware from '../middlewares/updateAdminMiddleware';

const router = Router();

router
  .get('/refresh', (req, res, next) => adminService.refresh(req, res)
    .catch(next))
  .get('/login', (req, res, next) => adminService.login(req, res)
    .catch(next))
  .get('/admin/:id', (req, res, next) => adminService.getUser(req, res)
    .catch(next))
  .get('/', (req, res, next) => adminService.getAdmins(req, res)
    .catch(next))
  .post('/register', registrMiddleware, (req, res, next) => adminService.register(req, res)
    .catch(next))
  .post('/hostlogin', (req, res, next) => adminService.hostLogin(req, res)
    .catch(next))
  .post('/signin', (req, res, next) => adminService.signin(req, res)
    .catch(next))
  .post('/signinEmail', (req, res, next) => adminService.signinEmail(req, res)
    .catch(next))
  .put('/', updateAdminMiddleware, (req, res, next) => adminService.updateAdmin(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => adminService.deleteAdmin(req, res)
    .catch(next));

export default router;
