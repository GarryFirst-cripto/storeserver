import { Router } from 'express';
import * as imageService from '../services/imageService';

const router = Router();

router
  .get('/:id', (req, res, next) => imageService.loadImage(req.params.id)
    .then(result => res.status(200).send(result))
    .catch(next));

export default router;
