import { Router } from 'express';
import * as naborItemService from '../services/naborItemService';

const router = Router();

router
  .get('/bygood/:id', (req, res, next) => naborItemService.getByGood(req, res)
    .catch(next))
  .post('/', (req, res, next) => naborItemService.createNaborItem(req, res)
    .catch(next))
  .put('/', (req, res, next) => naborItemService.updateNaborItem(req, res)
    .catch(next))
  .delete('/:id', (req, res, next) => naborItemService.deleteNaborItem(req, res)
    .catch(next));

export default router;
