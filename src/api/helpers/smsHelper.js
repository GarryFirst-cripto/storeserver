import { SMSRu } from 'node-sms-ru';
import { env } from '../../config/dbConfig';

const key = env.sms.apiKey;
const smsRu = new SMSRu(key);
const lp = String.fromCharCode(13, 10);

const sendSms = async (phone, kode) => {
  const message = `Доброго времени суток ! Ваш контрольный код : ${kode} .${lp} С наилучшими пожеланиями !`;
  const result = await smsRu.sendSms({
    from: 'Store App Server',
    to: phone,
    msg: message
  });
  const { status, sms } = result;
  return { status, sms };
};

export default async (phone, kode) => {
  try {
    const result = await sendSms(phone, kode);
    // const result = { phone, kode };
    return result;
  } catch (err) {
    return err;
  }
};
