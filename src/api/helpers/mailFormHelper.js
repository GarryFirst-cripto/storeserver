const docHeader = '<!DOCTYPE HTML> <html lang="ru"> <head> <meta charset="utf-8"/>'
  + ' <title> Message from Storte Server</title> </head> <body>';

const resetMessage = 'Доброго времени суток ! Ваш контрольный код :';

// const resetForm = kode => (
//   '<form style="width: 350px; margin-left: 50px;'
//   + ' padding: 15px; font-size: 18px; background-color: #94dcc0; border: 1px #002aff solid">'
//   + `<input type="text" name ="pass" readonly="readonly" style="background-color: #e7e7f6" value=${kode} >`
//   + '</form>'
// );

export default kode => (
  `${docHeader} <h3> ${resetMessage} ${kode} </h3> </body>`
);
