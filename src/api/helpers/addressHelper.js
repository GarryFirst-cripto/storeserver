
// eslint-disable-next-line import/no-mutable-exports
export let host = '';
export const setHost = value => {
  host = value.includes('localhost') ? `http://${value}/image/` : `https://${value}/image/`;
};
