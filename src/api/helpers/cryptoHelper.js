import jwt from 'jsonwebtoken';
import { env } from '../../config/dbConfig';

const { crypto: { secretKey: key, secretExp: period } } = env;

export const createAccessToken = data => jwt.sign(data, key, { expiresIn: period });
export const createRefreshToken = data => {
  const { id, mode: reff } = data;
  // const refData = { data: { id }, reff: true };
  return jwt.sign({ id, reff }, key);
};

export const fetchTokenData = token => {
  if (token) {
    try {
      return jwt.verify(token, key);
    } catch (e) {
      return null;
    }
  }
  return null;
};
