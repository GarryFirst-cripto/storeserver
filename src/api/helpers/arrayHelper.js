export const insertPicture = (oldList, item, index) => {
  if (oldList) {
    if (index) {
      const array = oldList.split(',');
      array.splice(index, 0, item);
      return array.join(',');
    }
    return `${oldList},${item}`;
  }
  return item;
};

export const removePicture = (oldList, item) => {
  if (oldList) {
    const array = oldList.split(',');
    for (let i = 0; i < array.length; i++) {
      if (array[i] === item) {
        array.splice(i, 1);
        break;
      }
    }
    return array.join(',');
  }
  return null;
};

export const parce = value => {
  const result = {};
  const array = value.split('&');
  for (let i = 0; i < array.length; i++) {
    const item = array[i].split('=');
    // eslint-disable-next-line prefer-destructuring
    result[item[0]] = item[1];
  }
  return result;
};
