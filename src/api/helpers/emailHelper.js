/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/* eslint-disable no-console */
import fs from 'fs';
import nodemailer from 'nodemailer';
import readline from 'readline';
import { google } from 'googleapis';
import { env } from '../../config/dbConfig';
import createMailText from './mailFormHelper';

const { smtpUser } = env.smtp;
const TOKEN_PATH = 'src/credentials/mail_token.json';
const CREDENTIALS_PATH = 'src/credentials/credentials.json';
const SCOPES = [
  'https://mail.google.com/',
  'https://www.googleapis.com/auth/gmail.modify',
  'https://www.googleapis.com/auth/gmail.compose',
  'https://www.googleapis.com/auth/gmail.send'
];

function getNewToken(oAuth2Client, credentials, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', code => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), error => {
        if (error) return console.error(error);
        console.log('Token stored to', TOKEN_PATH);
      });
      const { client_secret, client_id } = credentials.installed;
      const { refresh_token } = token;
      callback(oAuth2Client, refresh_token, client_secret, client_id);
    });
  });
}

function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, credentials, callback);
    oAuth2Client.setCredentials(JSON.parse(token.toString()));
    const { refresh_token } = JSON.parse(token.toString());
    callback(oAuth2Client, refresh_token, client_secret, client_id);
  });
}

let smtpTransport;

const accessToken = async oauth2Client => {
  const result = await oauth2Client.getAccessToken();
  return result;
};
async function createTransport(auth, refresh_token, client_secret, client_id) {
  const token = await accessToken(auth);

  smtpTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      type: 'OAuth2',
      user: smtpUser,
      clientId: client_id,
      clientSecret: client_secret,
      refreshToken: refresh_token,
      accessToken: token
    }
  });
  console.log('Smtp-Transport ready ...');
}

fs.readFile(CREDENTIALS_PATH, (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  authorize(JSON.parse(content.toString()), createTransport);
});

export default async (email, kode) => {
  const mailOptions = {
    from: smtpUser,
    to: email,
    subject: 'Your verification code',
    html: createMailText(kode)
  };
  const result = await smtpTransport.sendMail(mailOptions);
  return { result };
};
