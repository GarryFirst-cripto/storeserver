
const asyncForEach = async (fn, list) => {
  for (let i = 0; i < list.length; i++) {
    // eslint-disable-next-line no-await-in-loop
    await fn(list[i]);
  }
};

export default asyncForEach;
