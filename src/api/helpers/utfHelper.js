
const simbUTFa = ['\u0410', '\u0411', '\u0412', '\u0413', '\u0414', '\u0415', '\u0416', '\u0417', '\u0418', '\u0419', '\u041A', '\u041B', '\u041C', '\u041D', '\u041E', '\u041F',
  '\u0420', '\u0421', '\u0422', '\u0423', '\u0424', '\u0425', '\u0426', '\u0427', '\u0428', '\u0429', '\u042A', '\u042B', '\u042C', '\u042D', '\u042E', '\u042F',
  '\u0430', '\u0431', '\u0432', '\u0433', '\u0434', '\u0435', '\u0436', '\u0437', '\u0438', '\u0439', '\u043A', '\u043B', '\u043C', '\u043D', '\u043E', '\u041F',
  '\u0440', '\u0441', '\u0442', '\u0443', '\u0444', '\u0445', '\u0446', '\u0447', '\u0448', '\u0449', '\u044A', '\u044B', '\u044C', '\u044D', '\u044E', '\u044F'];

export function StrWinToUtf8(wintext) {
  let utftext = '';
  if (wintext && wintext.length > 0) {
    for (let n = 0; n < wintext.length; n++) {
      const c = wintext.charCodeAt(n);
      if (c < 128) {
        if (c === 13) {
          utftext += '';
        } else if (c === 9) {
          utftext += String.fromCharCode(160).repeat(9);
        } else {
          utftext += String.fromCharCode(c);
        }
      } else if ((c >= 1040) && (c <= 1103)) {
        utftext += simbUTFa[c - 1040];
      } else if (c === 1025) {
        utftext += '\u0401';
      } else if (c === 1105) {
        utftext += '\u0451';
      }
    }
  }
  return utftext;
}

// export function StrUtfToWin(unitext) {
//   let wintext = '';
//   let i = 0;
//   while (i < unitext.length) {
//     const c = unitext.charCodeAt(i);
//     if (c < 128) {
//       wintext += String.fromCharCode(c);
//     } else if ((c >= 0x0410) && (c <= 0x044f)) {
//       const code = c - 0x0410 + 192;
//       wintext += String.fromCharCode(code);
//     } else {
//       for (let n = 0; n < simbUTFa.length; n++) {
//         if (c === simbUTFa[n]) {
//           const code = n + 128;
//           wintext += String.fromCharCode(code);
//           break;
//         }
//       }
//     }
//     i += 1;
//   }
//   return wintext;
// }
