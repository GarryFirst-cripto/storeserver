import dotenv from 'dotenv';
import * as pgStringParser from 'pg-connection-string';

dotenv.config();

const herokuData = process.env.DATABASE_URL
  ? pgStringParser.parse(process.env.DATABASE_URL)
  : {};

const dialOptions = process.env.DATABASE_URL
  ? { ssl: { rejectUnauthorized: false } }
  : {};

export const env = {
  app: {
    port: process.env.APP_PORT,
    adress: process.env.APP_ADRESS,
    socketPort: process.env.SOCKET_PORT,
    secret: process.env.SECRET_KEY
  },
  db: {
    database: herokuData.database || process.env.DB_NAME,
    username: herokuData.user || process.env.DB_USERNAME,
    password: herokuData.password || process.env.DB_PASSWORD,
    host: herokuData.host || process.env.DB_HOST,
    port: herokuData.port || process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    ssl: true,
    dialectOptions: dialOptions,
    logging: false
  },
  smtp: {
    smtpUser: process.env.SMTP_USER
  },
  sms: {
    apiKey: process.env.SMS_API_KEY
  },
  crypto: {
    secretKey: process.env.SECRET_KEY,
    secretExp: process.env.SECRET_EXP
  },
  admin: {
    adminName: process.env.SYSTEM_ADMIN_NAME,
    adminPWD: process.env.SYSTEM_ADMIN_PWD
  },
  filesize: process.env.MAX_FILE_SIZE,
  port: process.env.PORT,
  use_env_variable: process.env.DATABASE_URL
};

export const { database, username, password, host, port, dialect, dialectOptions, logging } = env.db;
// export default env;
