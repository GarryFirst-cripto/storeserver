export default [
  '/api/users/signin',
  '/api/users/register',
  '/api/admins/hostlogin',
  '/api/admins/signin',
  '/api/admins/signinemail',
  '/image'
];
